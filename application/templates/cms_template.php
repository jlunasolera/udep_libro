<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="X-UA-Compatible" content="IE=11,chrome=1">
	<title>ADMINISTRADOR DE CONTENIDOS CMS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/bootstrap/dist/css/bootstrap.min.css"><!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/font-awesome/css/font-awesome.min.css"><!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/Ionicons/css/ionicons.min.css"><!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/pnotify/pnotify.custom.min.css"><!-- Notify -->
  <link rel="stylesheet" href="<?php echo site_url() ?>static/cms/lib/bootstrap-daterangepicker/daterangepicker.css"><!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo site_url() ?>static/cms/lib/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"><!-- bootstrap datepicker -->
	<link rel="stylesheet" href="<?php echo site_url() ?>static/cms/lib/datatables.net-bs/css/dataTables.bootstrap.min.css"><!-- DataTables -->
	<link rel="stylesheet" href="<?php echo site_url() ?>static/cms/lib/datatables.net/css/buttons.dataTables.min.css"><!-- DataTables -->
	<link rel="stylesheet" href="<?php echo site_url() ?>static/cms/lib/dropzone/dropzone.css"><!-- Dropzone -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/jvectormapcss/jquery-jvectormap.css"><!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/dist/css/AdminLTE.min.css"><!-- Theme style -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/lib/dist/css/skins/_all-skins.min.css"><!-- Skins a elegir style -->
	<link rel="stylesheet" href="<?php echo site_url()?>static/cms/css/main/main.min.css"><!-- Style custom -->

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet" type="text/css">

	<script src="<?php echo site_url()?>static/cms/lib/jquery/dist/jquery.min.js"></script><!-- jQuery 3 -->
	
	<script type="text/javascript">
		site = "<?php echo site_url(); ?>";
	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div id="main-wrapper" class="wrapper udepMain">
		<header class="main-header">
    	<!-- Logo -->
    	<a href="javascript:;" class="logo">
      	<!-- mini logo for sidebar mini 50x50 pixels -->
      	<span class="logo-mini"><img alt="Nextperience Admin" src="<?php echo site_url()?>static/cms/images/brand/logo_udep_small.png"></span>
      	<!-- logo for regular state and mobile devices -->
      	<!-- <span class="logo-lg"><b>Admin</b>LTE</span> -->
				<span class="logo-lg"><img alt="Nextperience Admin" src="<?php echo site_url()?>static/cms/images/brand/logo_udep_big.png"></span>
    	</a>

    	<!-- Header Navbar: style can be found in header.less -->
    	<nav class="navbar navbar-static-top">
      	<!-- Sidebar toggle button-->
      	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        	<span class="sr-only">Toggle navigation</span>
      	</a>
      	<!-- Navbar Right Menu -->
      	<div class="navbar-custom-menu">
        	<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo site_url()?>static/cms/lib/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
								<span class="hidden-xs"><?php echo $_SESSION['usercms']['email']; ?></span>
								<span class="hidden-xs"><?php echo $_SESSION['usercms']['rol'] == 1 ? '(admin)':''; ?></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-footer">
									<div class="pull-right">
										<a href="<?php echo site_url()?>dashboard/login/logout" class="btn btn-default btn-flat">Cerrar sesión</a>
									</div>
								</li>
							</ul>
						</li>
        	</ul>
      	</div>
    	</nav>
  	</header>

		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">MENÚ</li>
					<li class="<?php echo uri_string() == 'dashboard/index' ? 'active':'';?>">
						<a href="<?php echo site_url()."dashboard/index";?>">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
							<!-- <span class="pull-right-container">
								<small class="label pull-right bg-green">new</small>
							</span> -->
						</a>
					</li>
					<li class="<?php echo uri_string() == 'dashboard/reportes' ? 'active':'';?>">
						<a href="<?php echo site_url()."dashboard/reportes";?>">
							<i class="fa fa-file-text"></i> <span>Reportes</span>
						</a>
					</li>
					<?php if (isset($_SESSION['usercms']['rol']) &&  $_SESSION['usercms']['rol'] == 1) { ?>
					<li class="<?php echo uri_string() == 'dashboard/usuarios' ? 'active':'';?>">
						<a href="<?php echo site_url()."dashboard/usuarios";?>">
							<i class="fa fa-user"></i> <span>Usuarios</span>
						</a>
					</li>
					<?php } ?>
					<!-- <li class="<?php echo uri_string() == 'dashboard/graficos' ? 'active':'';?>">
						<a href="<?php echo site_url()."dashboard/graficos";?>">
							<i class="fa fa-pie-chart"></i> <span>Gráficos</span>
						</a>
					</li> -->
				</ul>
			</section>
		</aside>

		<div id="content-wrapper" class="content-wrapper">
			<?php echo $body;?>
		</div>

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0.0
			</div>
			<strong>Solera Mobile &copy; 2017.</strong> All rights reserved.
		</footer>

		<div id="main-menu-bg" class="control-sidebar-bg"></div>

	</div> <!-- / #main-wrapper -->

	<script src="<?php echo site_url()?>static/cms/lib/bootstrap/dist/js/bootstrap.min.js"></script><!-- Bootstrap 3.3.7 -->
	<script src="<?php echo site_url()?>static/cms/lib/fastclick/lib/fastclick.js"></script><!-- FastClick -->
	<script src="<?php echo site_url()?>static/cms/lib/dist/js/adminlte.min.js"></script><!-- AdminLTE App -->
	<script src="<?php echo site_url()?>static/cms/lib/jquery-sparkline/dist/jquery.sparkline.min.js"></script><!-- Sparkline -->
	<script src="<?php echo site_url()?>static/cms/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script><!-- jvectormap  -->
	<script src="<?php echo site_url()?>static/cms/lib/jvectormap/jquery-jvectormap-world-mill-en.js"></script><!-- jvectormap  -->
	<script src="<?php echo site_url()?>static/cms/lib/jquery-slimscroll/jquery.slimscroll.min.js"></script><!-- SlimScroll -->
	<script src="<?php echo site_url()?>static/cms/lib/chart.js/Chart.js"></script><!-- ChartJS -->
	<script src="<?php echo site_url()?>static/cms/lib/pnotify/pnotify.custom.min.js"></script><!-- pnotify -->
	<!-- <script src="<?php echo site_url()?>static/cms/lib/dist/js/pages/dashboard2.js"></script> -->
	<script src="<?php echo site_url()?>static/cms/lib/dist/js/demo.js"></script><!-- AdminLTE for demo purposes -->
	<script>
		var $url_site = 'http://servicelibro.udep.edu.pe';
		var site_url = "<?php echo site_url(); ?>";
	</script>
	<script src="<?php echo site_url()?>static/cms/js/main.js"></script><!-- main js -->
</body>
</html>