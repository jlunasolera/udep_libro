$.fn.serializeObject = function() {
	var o = Object.create(null),
			elementMapper = function(element) {
				element.name = $.camelCase(element.name);
				return element;
			},
			appendToResult = function(i, element) {
				var node = o[element.name];

				if ('undefined' != typeof node && node !== null) {
					o[element.name] = node.push ? node.push(element.value) : [node, element.value];
				} else {
					o[element.name] = element.value;
				}
			};

	$.each($.map(this.serializeArray(), elementMapper), appendToResult);
	return o;
};

var table_report = null;

;(function ($) {
	// clic en la vista reporte
	// $('.complaint-view').on('click', function(e){
	// 	var $id = $(this).parent().parent().find('input').val();
	// 	viewComplaint($id);
	// })
	
	$('#btnFilterReport').on('click', function(e){
		$(this).prop('disabled', true);
		filterReport();
  })

  $('.btn-action-complaint').on('click', function(e){
		$('.btn-action-complaint').prop('disabled', true);
    var $id = $(this).parent().find('input[id="report-id"]').val();
		var $idUser = $(this).parent().find('input[id="login-id"]').val();
		var $nameUser = $(this).parent().find('input[id="login-name"]').val();
    var $action = $(this).attr('data-action');
    
    subStatusComplaint($id,$idUser,$action,$nameUser);
	})
	
	$('#reservation').daterangepicker({
		locale: {
			format: 'YYYY-MM-DD'
		}
	});

	table_report = $('#reportDatatables').DataTable({
		searching: false,
		processing: true,
		serverSide: true,
		ordering: true,
		ajax:{
			url: site + 'dashboard/reports/reports_json_post',
			type: "POST",
			dataFilter: function(data){
				var json = jQuery.parseJSON( data );
        	json.recordsTotal = json.recordsTotal;
          json.recordsFiltered = json.recordsFiltered;
					json.data = json.data;
					
				console.log(json.data);
 
        return JSON.stringify( json ); // return JSON string
      },
			dataSrc: function (jsonData) {
			  return jsonData.data;
			}
		},
		columns: [
			{ 'data' : 'code'},
			{ 'data' : 'name_complete'},
			{ 'data' : 'email_child'},
			{ 'data' : 'complain'},
			{ 'data' : 'address_station'},
			{ 'data' : 'typeservice',
				'render': function(data, type, row){
					if(data == 1){
						return 'Servicio'
					}else{
						return 'Producto'
					}
				}
			},
			{ 'data' : 'user_id'},
			{ 'data' : 'sub_status'},
			{ 'data' : 'status'},
			{ "data" : 'id',
        "render": function (data, type, row) {
					return "<a class='btn btn-sm btn-danger complaint-view' onclick='viewComplaint(" + data + ")'><span class='btn-label icon fa fa-eye' aria-hidden='true'></span></a>";
				}
			}
		],
		language: {
			url: site+"static/cms/lib/datatables.net/Spanish.json",
		},
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'excel',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
				}
			},{
				extend: 'pdf',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
				}
			},{
				extend: 'print',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
				}
			},
			// ----- Ini de creacuon de boton
			// {
			// 	text: 'My button',
			// 	action: function (e, dt, node, config){
			// 		alert('Button activated');
			// 	}
			// }
			// ----- Fin de creacuon de boton
		],
		columnDefs: [
			{
				targets: [1,2,3,4,5,6,7,8,9],
				orderable: false
			}
		]
	})

})(jQuery);

function filterReport(){

	var $dataObject = $('#filterReport').serializeObject();

	var sedes = $("#stationAddress").val(),
			servicio = $("#complainType").val(),
			estado = $("#complainStatus").val(),
			subestado = $("#complainSubStatus").val(),
			ejecutivo = $("#complainExecutive").val(),
			reserva = $("#reservation").val();
			complain = $("#complainTypeComplain").val();

	var query = '?station_address='+sedes+'&complain_type='+servicio+'&complain_status='+estado+'&complain_substatus='+subestado+'&complain_executive='+ejecutivo+'&complain='+complain+'&complain_date='+reserva;

	if(table_report.ajax.url( site +'dashboard/reports/filter_report/'+query).load()){
		$('#btnFilterReport').prop('disabled', false);
	}
	

	// $.ajax({
	// 	url: site + 'dashboard/reports/filter_report',
	// 	type:'post',
	// 	dataType: "json",
	// 	data: $dataObject,
	// 	success: function(data){
	// 		var $data = data.report,
	// 				$html;
	// 		if(data.status){
	// 			$('#reportDatatables').find('tbody').html('');

	// 			for (var i = 0; i < $data.length; i++) {
	// 				var $userexec = $data[i].user_name === null ? '-' : $data[i].user_name,
	// 						$typeserv = $data[i].typeservice == 0 ? 'Producto' : 'Servicio',
	// 						$addressStation = $data[i].address_station;

	// 				switch ($data[i].sub_status) {
	// 					case '1':
	// 						$substatus = "Procede";
	// 						break;
	// 					case '2':
	// 						$substatus = "Rechazado";
	// 						break;
	// 					default:
	// 						$substatus = "-";
	// 				}

	// 				switch ($data[i].status) {
	// 					case '0':
	// 						$status = "Pendiente";
	// 						break;
	// 					case '1':
	// 						$status = "Atendido";
	// 						break;
	// 					default:
	// 						$status = "-";
	// 				}

	// 				$html += '<tr class="gradeA odd">';
	// 				$html += '<input type="hidden" value="'+ $data[i].id +'">';
	// 				$html += '<td>'+ $data[i].code +'</td>';
	// 				$html += '<td>'+ $data[i].name_complete + '</td>';
	// 				$html += '<td>'+ $data[i].email_child + '</td>';
	// 				$html += '<td>'+ $addressStation + '</td>';
	// 				$html += '<td>'+ $typeserv + '</td>';
	// 				$html += '<td>'+ $userexec + '</td>';
	// 				$html += '<td>'+ $substatus + '</td>';
	// 				$html += '<td>'+ $status + '</td>';
	// 				$html += '<td><a class="btn btn-sm btn-danger complaint-view" onclick="viewComplaint('+ $data[i].id +')">';
	// 				$html += '<span class="btn-label icon fa fa-eye" aria-hidden="true"></span>';
	// 				$html += '</a>';
	// 				$html += '</td>';
	// 				$html += '</tr>';
	// 			}

	// 			$('#reportDatatables').find('tbody').append($html);

	// 			$('#btnFilterReport').prop('disabled', false);
	// 		}else{
	// 			console.log('error');
	// 		}
			
	// 	},
	// 	error: function(jqhxr, textStatus, error){
	// 		console.log('error en el envio');
	// 		$('#btnFilterReport').prop('disabled', false);
	// 	}
	// });
}

function viewComplaint(id){
	$.ajax({
		url: site + 'dashboard/visitors/complaint_view',
		type:'post',
		dataType: "json",
		data: {
		  "id": id
		},
		success: function(data){
			var $dato = data[0];

			if (data.status){
        $('#report-id').val($dato.id);
        $('#report-code').text($dato.code);
        $('#report-name').find('p').text($dato.namechild_full);
        $('#report-dni').find('p').text($dato.numberdoc_child);
        $('#report-email').find('p').text($dato.email_child);
        $('#report-service').find('p').text($dato.typeservice);
        $('#report-location').find('p').text($dato.address_station);
        $('#report-mount').find('p').text($dato.claim_amount);
        $('#report-description').find('p').text($dato.typeservice_description);
        $('#report-pdf').find('a').text($dato.url_pdf);
        $('#report-pdf').find('a').attr('href',$url_site+$dato.url_pdf);
        $('#report-type').find('p').text($dato.complain);
        $('#report-detail').find('p').text($dato.complain_detail);
        $('#report-order').find('p').text($dato.complain_order);
				$('#report-requestaddress').find('p').text($dato.address_notify);
				
				$('#report-response_mailDay').find('p').text($dato.date_resolution);
				$('#report-response_mailEjecutive').find('p').text($dato.user_id);

				console.log($dato.status_number);

				if($dato.status_number == 1){
					$('#reportTableResponse').show();
				}else{
					$('#reportTableResponse').hide();
				}

				$('#report-response_mailStatus').find('p').html($dato.status);
				$('#report-response_mailSubstatus').find('p').html($dato.sub_status);
				$('#report-response_mailCco').find('p').text($dato.complaint_mail_cco);
				$('#report-response_mailText').find('p').html($dato.complaint_mail_text);

				$filesArray = $dato.complaint_mail_files;

				$('#report-response_mailFiles').find('ul').html('');

				if($filesArray != ''){
					$filesArray.forEach( function(valor, indice, array) {
						var $html;

						var $html = '<li>';
								$html += '<a href="'+ site_url +'uploads/attachments/'+ valor +'" target="_blank">'+ valor +'</a>';
								$html += '</li>';
						$('#report-response_mailFiles').find('ul').append($html);
						// console.log("En el índice " + indice + " hay este valor: " + valor);
					});
				}

				// $('#report-response_mailFiles').find('ul').html($dato.complaint_mail_files);

        if($dato.sub_status > 0){
          $('.modal-footer').hide()
        }else{
          $('.modal-footer').show()
        }

        $('#modalReportView').modal('show')
			} else {
        console.log("no se cargaron los datos");
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}

function subStatusComplaint(id,idUser,action,nameUser){
  console.log(id+' '+idUser+' '+action)
  $.ajax({
		url: site + 'dashboard/visitors/status_complaint',
		type:'post',
		dataType: "json",
		data: {
      "id": id,
      "id_user": idUser,
			"action": action,
			'user_name': nameUser
		},
		success: function(data){
			if (data.status){
        $('#myModal').modal('hide');
        $('.btn-action-complaint').prop('disabled', false);
        location.reload();
			} else {
        console.log("no se cargaron los datos");
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}

function filterExport(){
	var $dataObject = $('#filterReport').serializeObject();

  $.ajax({
		url: site + 'dashboard/visitors/filterexport',
		type:'post',
		dataType: "json",
		data: $dataObject,
		success: function(data){
			if (data.status){
        console.log("se descargo")
			} else {
        console.log("no se descargo");
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}