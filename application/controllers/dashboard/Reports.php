<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->get_sesion();
		$this->load->model('visitor_model','obj_visitor');
		$this->load->model('libro_model', 'obj_libro');
		$this->load->model('datatables_report_model','obj_datatables_report');
		$this->load->library('Classes/PHPExcel.php');
	}
	public function index() {
		$objSedes = $this->obj_libro->list_sedes();
		$objUsers = $this->obj_libro->select_users();

		$this->cms_tmp_master->set('objUsers', $objUsers);
		$this->cms_tmp_master->set('objSedes', $objSedes);
		$this->cms_tmp_master->render('dashboard/report');
	}

	public function reports_json_post() {
		$columns = array(
      0 => 'code',
      1 => 'lastnamefa_child',
			2 => 'lastnamema_child',
			3 => 'name_child',
			4 => 'email_child',
			5 => 'address_station',
			6 => 'typeservice',
			7 => 'user_name',
			8 => 'sub_status',
			9 => 'status',
			10 => 'complain'
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');

		$arrayOrder = $this->input->post('order');
		$arraySearch = $this->input->post('search');

		$order = $columns[$arrayOrder[0]['column']];
		$dir = $arrayOrder[0]['dir'];

		$totalData = $this->obj_datatables_report->allposts_count();
		$totalFiltered = $totalData;

		if(empty($arraySearch['value'])){

			$posts = $this->obj_datatables_report->allposts($limit,$start,$order,$dir);

		} else {

			$search = $arraySearch['value'];
			$posts = $this->obj_datatables_report->posts_search($limit,$start,$search,$order,$dir);
			$totalFiltered = $this->obj_datatables_report->posts_search_count($search);

		}

		$data = array();

		if(!empty($posts)){
			foreach ($posts as $post){

				$nestedData['id'] = $post->id;
				$nestedData['code'] = $post->code;
				$nestedData['name_complete'] = $post->name_complete;
				$nestedData['email_child'] = $post->email_child;
				$nestedData['address_station'] = $post->address_station;
				$nestedData['typeservice'] = $post->typeservice;
				$nestedData['user_id'] = $post->user_id;

				switch ($post->complain){
					case 0:
						$nestedData['complain'] = 'Reclamo';
						break;
					case 1:
						$nestedData['complain'] = 'Queja';
						break;
				}

				switch ($post->sub_status) {
					case 0:
						$nestedData['sub_status'] = '<span>-</span>';
						break;
					case 1:
						$nestedData['sub_status'] = '<span class="label label-success">Aprobado</span>';
						break;
					case 2:
						$nestedData['sub_status'] = '<span class="label label-danger">Rechazado</span>';
						break;
					default:
						$nestedData['sub_status'] = '<span>-</span>';
						break;
				}
				switch ($post->status) {
					case 0:
						$nestedData['status'] = '<span class="label label-info">Pendiente</span>';
						break;
					case 1:
						$nestedData['status'] = '<span class="label label-primary">Finalizado</span>';
						break;
					default:
						$nestedData['status'] = '<span>-</span>';
						break;
				}
				$data[] = $nestedData;
			}
		}
			
		$json_data = array(
			"draw"            => intval($this->input->post('draw')),  
			"recordsTotal"    => intval($totalData),  
			"recordsFiltered" => intval($totalFiltered), 
			"data"            => $data   
		);
				
		echo json_encode($json_data);
	}

	public function complaint_view() {
		$id = $_POST["id"];

		$data = $this->obj_visitor->complain_detail($id);
		$data["status"] = true;

		echo json_encode($data);
	}

	public function filter_report($all = false) {
		$this->load->helper('text');

		$columns = array(
      0 => 'code',
      1 => 'lastnamefa_child',
			2 => 'lastnamema_child',
			3 => 'name_child',
			4 => 'email_child',
			5 => 'address_station',
			6 => 'typeservice',
			7 => 'user_name',
			8 => 'sub_status',
			9 => 'status',
			10 => 'complain'
		);

		$filter = array();

		if($this->input->get('station_address', TRUE) != ''){
			$filter["address_station"] = $this->input->get('station_address', TRUE);
		}
		if($this->input->get('complain_type', TRUE) != ''){
			$filter["typeservice"] = $this->input->get('complain_type', TRUE);
		}
		if($this->input->get('complain_status', TRUE) != ''){
			$filter["status"] = $this->input->get('complain_status', TRUE);
		}
		if($this->input->get('complain_substatus', TRUE) != ''){
			$filter["sub_status"] = $this->input->get('complain_substatus', TRUE);
		}
		if($this->input->get('complain_executive', TRUE) != ''){
			$filter["user_id"] = $this->input->get('complain_executive', TRUE);
		}

		if($this->input->get('complain', TRUE) != ''){
			$filter["complain"] = $this->input->get('complain', TRUE);
		}

		if($this->input->get('complain_date', TRUE) != ''){
			$complaindate = $this->input->get('complain_date', TRUE);

			$filter["complaindate"] = $complaindate;
			$filter["dateini"] = substr($complaindate, 0, -13);
			$filter["dateend"] = substr($complaindate, 13);
		}

		$limit = $this->input->post('length');
		$start = $this->input->post('start');

		$arrayOrder = $this->input->post('order');

		$order = $columns[$arrayOrder[0]['column']];
		$dir = $arrayOrder[0]['dir'];

		$totalData = $this->obj_datatables_report->allposts_count();
		$totalFiltered = $this->obj_visitor->filter_report_count($filter);

		$posts = $this->obj_visitor->filter_report($filter,$limit,$start,$order,$dir);

		$data = array();
		
		if(!empty($posts)){
			foreach ($posts as $post){

				$nestedData['id'] = $post->id;
				$nestedData['code'] = $post->code;
				$nestedData['name_complete'] = $post->name_complete;
				$nestedData['email_child'] = $post->email_child;
				$nestedData['address_station'] = $post->address_station;
				$nestedData['typeservice'] = $post->typeservice;
				$nestedData['user_id'] = empty($post->user_id) ? '-':$post->user_id;

				switch ($post->complain){
					case 0:
						$nestedData['complain'] = 'Reclamo';
						break;
					case 1:
						$nestedData['complain'] = 'Queja';
						break;
				}

				switch ($post->sub_status) {
					case 0:
						$nestedData['sub_status'] = '<span>-</span>';
						break;
					case 1:
						$nestedData['sub_status'] = '<span class="label label-success">Aprobado</span>';
						break;
					case 2:
						$nestedData['sub_status'] = '<span class="label label-danger">Rechazado</span>';
						break;
					default:
						$nestedData['sub_status'] = '<span>-</span>';
						break;
				}
				switch ($post->status) {
					case 0:
						$nestedData['status'] = '<span class="label label-info">Pendiente</span>';
						break;
					case 1:
						$nestedData['status'] = '<span class="label label-primary">Finalizado</span>';
						break;
					default:
						$nestedData['status'] = '<span>-</span>';
						break;
				}
				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),  
			"recordsTotal"    => intval($totalData),  
			"recordsFiltered" => intval($totalFiltered), 
			"data"            => $data   
		);
				
		echo json_encode($json_data);
	}
}