<?php
class Visitor_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function insert_visitor(){
    $data['fullname'] = $this->input->post('fullnames');
    $data['dni'] = $this->input->post('dni');
    $data['email'] = $this->input->post('email');
    $data['number_assistant'] = $this->input->post('number_assistant');
    $data['subscription'] = $this->input->post('subscription') == "" ? "0":"1";
    $data['date_creation'] = date('Y-m-d H:i:s');

    $this->db->insert('visitors', $data);
    
    //Obteniendo el último código generado
    $visitor_id = $this->db->insert_id();

    return $visitor_id;
  }

  public function insert_type_users($code){
    $data['name'] = $this->input->post('newuser_name');
    $data['lastname'] = $this->input->post('newuser_lastname');
    $data['email'] = $this->input->post('newuser_email');
    $data['status'] = 1;
    $data['rol'] = $this->input->post('newuser_typerol');
    $data['create_at'] = date('Y-m-d H:i:s');
    $data['password'] = md5(get_semilla().$code);

    $this->db->insert('user', $data);
    
    //Obteniendo el último código generado
    $user_id = $this->db->insert_id();

    return $user_id;
  }

  public function paged_list($start, $end, $type = null, $where = null){
    $this->db->select('id,name_child,lastnamefa_child,lastnamema_child,email_child,date_creation,CASE WHEN date_resolution IS NULL THEN "-" ELSE date_resolution END AS date_resolution,code,sedes.name AS address_station,reclamo.status AS status,CASE WHEN CONCAT(user.name," ",user.lastname) IS NULL THEN "-" ELSE CONCAT(user.name," ",user.lastname) END AS user_name,typeservice,typedoc_child,numberdoc_child,phone_child,department_child,province_child,district_child,address_child,name_tutor,lastnamefa_tutor,lastnamema_tutor,typedoc_tutor,numberdoc_tutor,phone_tutor,email_tutor,department_tutor,province_tutor,district_tutor,address_tutor,younger,complain,sub_status,claim_amount,typeservice_description,complain_detail,complain_order',FALSE);
    $this->db->from('reclamo');
    $this->db->join('sedes', 'sedes.idSedes = reclamo.address_station');
    $this->db->join('user', 'user.user_id = reclamo.user_id', 'left');
    $this->db->order_by('date_creation DESC');

    // $sql='SELECT id, name_child, lastnamefa_child, lastnamema_child, email_child, date_creation, date_resolution, code, sedes.name AS address_station, reclamo.status, CASE WHEN (select CONCAT(user.name, " ", user.lastname) FROM `user` WHERE `user`.`user_id` = `reclamo`.`user_id`) IS NULL THEN "-" ELSE (select CONCAT(user.name, " ", user.lastname) FROM `user` WHERE `user`.`user_id` = `reclamo`.`user_id`) END AS user_name, typeservice FROM (`reclamo`) JOIN `sedes` ON `sedes`.`idSedes` = `reclamo`.`address_station` ORDER BY `date_creation` DESC LIMIT 10';

    // $query_visitors = $this->db->query($sql);

    $query_visitors = $this->db->get('', $end, $start);

    return $query_visitors->result();
  }

  public function users_list($start, $end, $type = null, $where = null){
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('status <> 2');
    $this->db->order_by('name ASC');

    $query_visitors = $this->db->get('', $end, $start);

    return $query_visitors->result();
  }

  public function users_status(){
    $this->db->select('*');
    $this->db->from('user');
    $this->db->where('status', 1);
    $this->db->order_by('name ASC');

    $query_users = $this->db->get();

    return $query_users->result();
  }

  public function update($id_row, $data = array()) {
    $this->db->where('visitor_id', $id_row);
    $this->db->update('visitors', $data); 
      
    // Obtiene las filas afectadas por efecto de la consulta
    $afected = $this->db->affected_rows();

    // Obtiene el resultado de la consulta
    return ($afected > 0 ? $id_row: false);
  }

  public function delete_users($id){
    $data = array(
      'status' => 2,
    );

    $this->db->where('user_id', $id);
    $this->db->update('user', $data); 

    return true;
  }

  public function change_status($id,$status){

    if($status == 1){
      $data = array(
        'status' => 0,
      );
    }else{
      $data = array(
        'status' => 1,
      );
    }
    
    $this->db->where('user_id', $id);
    $this->db->update('user', $data);

    return true;
  }

  public function filter_report($filter,$limit,$start,$col,$dir){
    $this->db->select("id, code, CONCAT(name_child,' ',lastnamefa_child,' ',lastnamema_child) AS name_complete, email_child, sedes.name AS address_station, typeservice, CONCAT(user.name,' ',user.lastname) AS user_id, sub_status, reclamo.status, complain",FALSE);
    $this->db->limit($limit,$start);
    $this->db->order_by($col,$dir);
    $this->db->join('sedes', 'sedes.idSedes = reclamo.address_station','left');
    $this->db->join('user', 'user.user_id = reclamo.user_id','left');

    if(isset($filter)){
      if(isset($filter['address_station'])){
        $this->db->where_in('reclamo.address_station',$filter['address_station']);
      }
      if(isset($filter['typeservice'])){
        $this->db->where_in('reclamo.typeservice',$filter['typeservice']);
      }
      if(isset($filter['status'])){
        $this->db->where_in('reclamo.status',$filter['status']);
      }
      if(isset($filter['sub_status'])){
        $this->db->where_in('reclamo.sub_status',$filter['sub_status']);
      }
      if(isset($filter['user_id'])){
        $this->db->where_in('reclamo.user_id',$filter['user_id']);
      }
      if(isset($filter['complain'])){
        $this->db->where_in('reclamo.complain',$filter['complain']);
      }
      if(isset($filter['complaindate'])){
        $this->db->where('date_creation BETWEEN "'.$filter["dateini"].' 00:00:00" AND "'.$filter["dateend"].' 23:59:59"');
      }
    }

    $this->db->order_by('code ASC');

    $query = $this->db->get('reclamo');

    return $query->result();
  }

  public function filter_report_count($filter){
    $this->db->select("id, code, CONCAT(name_child,' ',lastnamefa_child,' ',lastnamema_child) AS name_complete, email_child, sedes.name AS address_station, typeservice, CONCAT(user.name,' ',user.lastname) AS user_name, user.user_id, sub_status, reclamo.status, complain",FALSE);
    $this->db->join('sedes', 'sedes.idSedes = reclamo.address_station','left');
    $this->db->join('user', 'user.user_id = reclamo.user_id','left');

    if(isset($filter)){
      if(isset($filter['address_station'])){
        $this->db->where_in('reclamo.address_station',$filter);
      }
      if(isset($filter['typeservice'])){
        $this->db->where_in('reclamo.typeservice',$filter);
      }
      if(isset($filter['status'])){
        $this->db->where_in('reclamo.status',$filter);
      }
      if(isset($filter['sub_status'])){
        $this->db->where_in('reclamo.sub_status',$filter);
      }
      if(isset($filter['user_id'])){
        $this->db->where_in('reclamo.user_id',$filter);
      }
      if(isset($filter['complain'])){
        $this->db->where_in('reclamo.complain',$filter);
      }
      if(isset($filter['complaindate'])){
        $this->db->where('date_creation BETWEEN "'.$filter["dateini"].' 00:00:00" AND "'.$filter["dateend"].' 23:59:59"');
      }
    }

    $this->db->order_by('code ASC');

    $query = $this->db->get('reclamo');

    return $query->num_rows();
  }

  public function complain_detail($id){
    $this->db->select('*');
    $this->db->from('reclamo');
    $this->db->where('id', $id);

    $query = $this->db->get();

    return $query->result();
  }

  public function change_status_complaint($id,$idUser,$action,$nameUser){
    if($action > 0) {
      $data = array (
        'status' => 1,
        'sub_status' => $action,
        'user_id' => $idUser,
        'user_name' => $nameUser
      );
    }
    
    $this->db->where('id', $id);
    $this->db->update('reclamo', $data);

    return true;
  }

  public function change_password($code,$id){

    $data['password'] = md5(get_semilla().$code);

    $this->db->where('user_id',$id);

    if($this->db->update('user', $data)){
      return true;
    }else{
      return false;
    }
  }

  public function user_email($id){
    $this->db
            ->select('email')
            ->where('user_id',$id)
            ->limit('1');

    $query = $this->db->get('user');

    foreach ($query->result() as $row){
      $email = $row->email;
    }

    return $email;
  }

  public function complaint_mail_files($id,$files){

    $data = array();

    $this->db->select('complaint_mail_files');
    $this->db->where('id', $id);
    $query = $this->db->get('reclamo');

    foreach ($query->result() as $row){
      $datapo = $row->complaint_mail_files;

      if($datapo){
        $datapoadd = $datapo.",".$files;
      }else{
        $datapoadd = $files;
      }

      $data = array(
        'complaint_mail_files' => $datapoadd
      );
    }

    $this->db->where('id', $id);
    $this->db->update('reclamo', $data);

    return true;
  }

  public function complaint_form_rejected($id,$message,$cco,$action,$user_id){
    switch ($action) {
			case 0:
        $data = array(
          'complaint_mail_cco' => $cco,
          'complaint_mail_text' => $message,
          'sub_status' => $action
        );
				break;
			case 1:
        $data = array(
          'complaint_mail_cco' => $cco,
          'complaint_mail_text' => $message,
          'sub_status' => $action,
          'user_id' => $user_id,
          'status' => 1,
          'date_resolution' => date("Y-m-d H:i:s")
        );
				break;
			case 2:
        $data = array(
          'complaint_mail_cco' => $cco,
          'complaint_mail_text' => $message,
          'sub_status' => $action,
          'user_id' => $user_id,
          'status' => 1,
          'date_resolution' => date("Y-m-d H:i:s")
        );
				break;
		}

    $this->db->where('id', $id);
    $this->db->update('reclamo', $data);

    return true;
  }

  public function complaint_attach($id){
    $this->db
          ->select('complaint_mail_files')
          ->where('id', $id);
    $query = $this->db->get('reclamo');

    foreach ($query->result() as $row){
      $files = $row->complaint_mail_files;
    }

    return $files;
  }

  public function users_name($id){
    $this->db->select('name,lastname');
    $this->db->where('user_id',$id);
    $query_users = $this->db->get('user');
    foreach ($query_users->result() as $row){
      $name = $row->name.' '.$row->lastname;
    }
    return $name;
  }

  public function sedes_name($id){
    $this->db->select('*');
    $this->db->where('idSedes',$id);
    $query_sedes = $this->db->get('sedes');

    foreach ($query_sedes->result() as $row){
      $name = $row->name;
    }
    return $name;
  }

  public function remove_files_save($into,$id){
    $data = array (
      'complaint_mail_files' => $into
    );

    $this->db->where('id', $id);
    $this->db->update('reclamo', $data);

    $this->db->select('complaint_mail_files');
    $this->db->where('id',$id);
    $query_sedes = $this->db->get('reclamo');

    foreach ($query_sedes->result() as $row){
      $files = $row->complaint_mail_files;
      $file = explode( ',' , $files );
    }
    return $file;
  }

  public function response_email_tutor($id){

    $this->db->select('email_tutor');
    $this->db->where('id',$id);
    $query_email = $this->db->get('reclamo');

    foreach ($query_email->result() as $row){
      $files = $row->email_tutor;
    }
    return $files;
  }

  public function response_ubigeo($id){
    $this->db->select('*');
    $this->db->where('ubigeo_id',$id);
    $query_ubigeo = $this->db->get('ubigeo');

    foreach ($query_ubigeo->result() as $row){
      $ubi = $row->name;
    }
    return $ubi;
  }

  // public function response_station($id){
  //   $this->db->select('*');
  //   $this->db->where('idSedes',$id);
  //   $query_sedes = $this->db->get('sedes');

  //   foreach ($query_sedes->result() as $row){
  //     $sedes = $row->name;
  //   }
  //   return $sedes;
  // }
  
}