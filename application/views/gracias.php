<div class="main main-<?php echo $data["institution"] ?>">
  <div class="navbar">
    <div class="container">
      <div class="grid">
        <div class="grid-row">
          <div class="grid-box grid-box-6">
            <div class="brand"><a class="brand-box" href="javascript:;"><img src="<?php echo site_url('static/web/dist'); ?>/images/<?php echo $data["institution"] ?>/brand_<?php echo $data["institution"] ?>.png" alt="<?php echo $data["institution"] ?>"></a></div>
          </div>
          <div class="grid-box grid-box-6">
            <div class="subject">
              <h1>Hoja de reclamación</h1>
              <h3>UNIVERSIDAD DE PIURA - RUC: 20172627421</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="thanks-content">
      <div class="thanks-content-head">
        <h3>El formulario fue enviado correctamente</h3>
        <h4>Nº <?php echo $data["num_order"] ?></h4>
      </div>
      <div class="thanks-content-text">
        <p>Estimado(a) usuario,</p>
        <p>Agradeceremos su contacto, acusamos recibo de su carta que será asignada a un ejecutivo para su investigación y respuesta tan pronto reunamos los antecedentes.</p>
        <p>Atentamente,</p>
        <p>Atención al cliente</p>
        <p>Universidad de Piura</p>
      </div>
      <div class="thanks-content-footer">
        <div class="footer-content">
          <div class="footer-content-box">
            <a href="<?php echo site_url(); ?>" class="footer-box-back">REGRESAR AL HOME</a>
          </div>
          <div class="footer-content-box">
            <a href="http://servicelibro.udep.edu.pe<?php echo $data["url_pdf"] ?>" target="_blank" class="footer-box-pdf">DESCARGAR PDF</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>