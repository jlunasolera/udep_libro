<?php

class Ubigeo_model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }

  public function listParent() {
    $this->db->select('ubigeo_id, name');
    $this->db->where('parent_id = 0');
    $this->db->from('ubigeo');
    $query = $this->db->get('');
    return $query->result();
  }

  public function listUbigeot($parentId) {
    $this->db->select('ubigeo_id,name');
    $this->db->where('parent_id = '.$parentId);
    $this->db->from('ubigeo');
    $query = $this->db->get('');
    return $query->result();
  }

}

?>