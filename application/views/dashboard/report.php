<section class="content-header">
	<h1>Reportes</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-sm-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Filtros</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<form id="filterReport" class="filterReport">
							<div class="col-sm-3">
								<div class="form-group">
									<select name="station_address" id="stationAddress" class="form-control">
										<option value>- Seleciona Sede -</option>
										<?php if(isset($objSedes)) {
                      foreach($objSedes as $value) {?>
                        <option value="<?php echo $value->idSedes ?>"><?php echo $value->name ?></option>
                      <?php }
                    } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<select name="complain_type" id="complainType" class="form-control">
										<option value>- Seleccione Tipo de Servicio -</option>
										<option value="0">Producto</option>
										<option value="1">Servicio</option>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<select name="complain_status" id="complainStatus" class="form-control">
										<option value>- Seleccione estado -</option>
										<option value="0">Pendiente</option>
										<option value="1">Atendido</option>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<select name="complain_substatus" id="complainSubStatus" class="form-control">
										<option value>- Sub-Estado -</option>
										<option value="1">Aprobado</option>
										<option value="2">Rechazado</option>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<select name="complain_executive" id="complainExecutive" class="form-control">
										<option value>- Responsable -</option>
										<?php if(isset($objUsers)) {
                      foreach($objUsers as $value) {?>
                        <option value="<?php echo $value->user_id ?>"><?php echo $value->name.' '.$value->lastname ?></option>
                      <?php }
                    } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<select name="complain_typecomplain" id="complainTypeComplain" class="form-control">
										<option value>- Tipo de reclamo -</option>
										<option value="0">Reclamo</option>
										<option value="1">Queja</option>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="reservation" name="complain_calendar">
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<button type="button" id="btnFilterReport" class="btn btn-primary btn-block"><span class="btn-label icon fa fa-search" ></span>&nbsp;&nbsp;Buscar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<div class="box">
				<div class="box-body">
					<table id="reportDatatables" class="table table-bordered table-hover" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Número</th>
								<th>Nombres y Apellidos</th>
								<th>Correo</th>
								<th>Tipo de reclamo</th>
								<th>Sede</th>
								<th>Tipo de servicio</th>
								<th>Responsable</th>
								<th>Sub Estado</th>
								<th>Estado</th>
								<th>Acción</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- modal vista -->
<div id="modalReportView" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Reclamo nº <span id="report-code"></span></h4>
			</div>
			<div class="modal-body">
				<table class="report-table">
					<tbody>
						<tr>
							<td>
								<table class="table table-striped report-table-striped">
									<thead>
										<tr>
											<th colspan="2">
												<div class="report-view-detail-title">
													<h3>Identificación del consumidor contratante</h3>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr id="report-name">
											<td class="report-table-label"><h4>Nombre completo:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-dni">
											<td class="report-table-label"><h4>DNI:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-email">
											<td class="report-table-label"><h4>E-mail:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-pdf">
											<td class="report-table-label"><h4>Pdf Generado:</h4></td>
											<td>
												<p>
													<span class="label label-primary bg-teal">
														<a href="https://qa-udep.ws.solera.pe/pdf/HyJ7cPkkz.pdf" target="_blank">/pdf/HyJ7cPkkz.pdf</a>
													</span>
												</p>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="table table-striped report-table-striped">
									<thead>
										<tr>
											<th colspan="2">
												<div class="report-view-detail-title">
													<h3>Identificación del bien contratado</h3>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr id="report-service">
											<td class="report-table-label"><h4>Bien contratado:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-location">
											<td class="report-table-label"><h4>Sede:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-mount">
											<td class="report-table-label"><h4>Monto reclamado:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-description">
											<td class="report-table-label"><h4>Decripción:</h4></td>
											<td><p></p></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="table table-striped report-table-striped">
									<thead>
										<tr>
											<th colspan="2">
												<div class="report-view-detail-title">
													<h3>Detalle de la reclamación</h3>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr id="report-type">
											<td class="report-table-label"><h4>Tipo de reclamo:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-detail">
											<td class="report-table-label"><h4>Detalle:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-order">
											<td class="report-table-label"><h4>Pedido:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-requestaddress">
											<td class="report-table-label"><h4>Dirección de respuesta:</h4></td>
											<td><p></p></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table class="table table-striped report-table-striped" id="reportTableResponse">
									<thead>
										<tr>
											<th colspan="2">
												<div class="report-view-detail-title">
													<h3>Detalle de la respuesta</h3>
												</div>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr id="report-response_mailDay">
											<td class="report-table-label"><h4>Fecha de proceso:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-response_mailEjecutive">
											<td class="report-table-label"><h4>Responsable:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-response_mailStatus">
											<td class="report-table-label"><h4>Estado:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-response_mailSubstatus">
											<td class="report-table-label"><h4>Sub estado:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-response_mailCco">
											<td class="report-table-label"><h4>Correos en copia:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-response_mailText">
											<td class="report-table-label"><h4>Detalle:</h4></td>
											<td><p></p></td>
										</tr>
										<tr id="report-response_mailFiles">
											<td class="report-table-label"><h4>Archivos:</h4></td>
											<td>
												<ul>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- date-range-picker -->
<script src="<?php echo site_url() ?>static/cms/lib/moment/min/moment.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo site_url() ?>static/cms/lib/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- DataTables -->
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/dataTables.buttons.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net-bs/js/buttons.bootstrap.min.js"></script>

<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/buttons.flash.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/jszip.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/pdfmake.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/vfs_fonts.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/buttons.html5.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/lib/datatables.net/js/buttons.print.min.js"></script>

<script src="<?php echo site_url() ?>static/cms/js/reports.js"></script>