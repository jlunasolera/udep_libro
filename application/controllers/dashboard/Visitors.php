<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitors extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->get_sesion();
		$this->load->model('visitor_model','obj_visitor');
		$this->load->library('Classes/PHPExcel.php');
	}

	public function index() {

		$this->db->select('id');
		$this->db->from('reclamo');

		$query = $this->db->get('');

		$start = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$opciones = array();
		$opciones['per_page'] = 10;
		$opciones['base_url'] = site_url().'/dashboard/'.$this->uri->segment(2);
		$opciones['total_rows'] = $query->num_rows();
		$opciones['uri_segment'] = 3;
		$opciones['first_tag_open'] = '<li>';
		$opciones['first_tag_close'] = '</li>';
		$opciones['prev_tag_open'] = '<li>';
		$opciones['prev_tag_close'] = '</li>';
		$opciones['num_tag_open'] = '<li>';
		$opciones['num_tag_close'] = '</li>';
		$opciones['cur_tag_open'] = '<li class="active"><a>';
		$opciones['cur_tag_close'] = '</li></a>';
		$opciones['next_tag_open'] = '<li>';
		$opciones['next_tag_close'] = '</li>';
		$opciones['last_tag_open'] = '<li>';
		$opciones['last_tag_close'] = '</li>';

		$this->pagination->initialize($opciones);
		$obj_visitors = $this->obj_visitor->paged_list($start, $opciones['per_page'], 1);

		$this->db->select('id');
		$this->db->from('reclamo');
		$this->db->where('address_station', 1);
		$querysurco = $this->db->get();

		$this->db->select('id');
		$this->db->from('reclamo');
		$this->db->where('address_station', 2);
		$querymiraflores = $this->db->get();

		$this->db->select('id');
		$this->db->from('reclamo');
		$this->db->where('address_station', 3);
		$querypiura = $this->db->get();

		$this->db->select('id');
		$this->db->from('reclamo');
		$this->db->where('address_station', 4);
		$querychiclayo = $this->db->get();
		
		$this->db->select('id');
		$this->db->from('reclamo');
		$this->db->where('address_station', 5);
		$querytrujillo = $this->db->get();

		$this->cms_tmp_master->set('obj_visitors',$obj_visitors);
		$this->cms_tmp_master->set('obj_querysurco',$querysurco->num_rows());
		$this->cms_tmp_master->set('obj_querymiraflores',$querymiraflores->num_rows());
		$this->cms_tmp_master->set('obj_querypiura',$querypiura->num_rows());
		$this->cms_tmp_master->set('obj_querychiclayo',$querychiclayo->num_rows());
		$this->cms_tmp_master->set('obj_querytrujillo',$querytrujillo->num_rows());
		$this->cms_tmp_master->set('obj_paginacion',$this->pagination->create_links());
		$this->cms_tmp_master->render('dashboard/visitors');
	}

	public function newUserValidate () {
		if( $this->input->is_ajax_request() ){
			$this->form_validation->set_rules('newuser_name', 'Nombre', 'required|trim');
			$this->form_validation->set_rules('newuser_lastname', 'Apellido', 'required|trim');
			$this->form_validation->set_rules('newuser_email','Email','required|trim|valid_email');
			$this->form_validation->set_rules('newuser_typerol', 'Tipo de rol', 'numeric|trim|min_length[1]|max_length[2]');

			$this->form_validation->set_message('required','Campo requerido %s.');
			$this->form_validation->set_message('validar_email','dirección de email no es correcta en %s.');
			$this->form_validation->set_message('min_length','Campo %s no es correcto.');
			$this->form_validation->set_message('max_length','Campo %s no es correcto.');
			$this->form_validation->set_message('is_unique','Campo %s no esta disponible.');
			$this->form_validation->set_message('numeric','Campo %s debe ser numérico.');
			$this->form_validation->set_message('in_list','Campo %s es requerido.');

			if ( $this->form_validation->run($this) == false){
				$cadena  = explode("</p>", validation_errors());
				$cadena2 = implode("<p>", $cadena);
				$cadena3 = explode("<p>", $cadena2);
				array_pop($cadena3);
				array_shift($cadena3);
				$data['message'] = @$cadena3[0];
				$data['print'] = "false";

			} else {

				$this->load->helper('string');
				$pass_generate = random_string('alnum',6);
	
				$data['pass'] = $pass_generate;
				
				$obj_visitor = $this->obj_visitor->insert_type_users($pass_generate);

				if($obj_visitor > 0){
					$data['message'] = 'Insertado correctamente.';
					$data['print'] = true;
				// 	$data_mail['names'] = $this->input->post('fullnames');

					$email = $this->input->post('newuser_email');
					$content = $this->load->view("mail_register", $data, true);

					$this->sendMail($email,$content);

				} else {
					$data['message'] = "No se pudo realizar el registro, intentelo nuevamente en unos minutos.";
					$data['print'] = "false";
				}
			}

			echo json_encode($data);
			exit();
		}
	}

	public function changePassword () {

		$id = $_POST["id"];
		
		$this->load->helper('string');

		$pass_generate = random_string('alnum',6);

		$data['pass'] = $pass_generate;

		if($this->obj_visitor->change_password($pass_generate,$id)){
			$data['message'] = 'Se envío una nueva contraseña al correo del usuario.';
			$data['status'] = true;

			$email = $this->obj_visitor->user_email($id);
			$content = $this->load->view("mail_register", $data, true);

			$this->sendMail($email,$content);

		} else {
			$data['message'] = "No se pudo realizar el registro, intentelo nuevamente en unos minutos.";
			$data['status'] = false;
		}

		echo json_encode($data);
		exit();
	}

	public function export() {
		$obj_registros = $obj_visitors = $this->obj_visitor->paged_list(0, 0, 0);
		$headers = ''; // just creating the var for field headers to append to below
		$data = ''; // just creating the var for field data to append to below
		$filename = 'listado';
		
		$headers .= 'CÓDIGO'. "\t";
		$headers .= 'NOMBRES Y APELLIDOS'. "\t";
		$headers .= 'TIPO DE DOCUMENTO'. "\t";
		$headers .= 'NÚMERO DE DOCUMENTO'. "\t";
		$headers .= 'TELÉFONO'. "\t";
		$headers .= 'CORREO'. "\t";
		$headers .= 'DEPARTAMENTO'. "\t";
		$headers .= 'PROVINCIA'. "\t";
		$headers .= 'DISTRITO'. "\t";
		$headers .= 'DIRECCIÓN'. "\t";

		$headers .= 'NOMBRE Y APELLIDOS TUTOR'. "\t";
		$headers .= 'TIPO DE DOCUMENTO TUTOR'. "\t";
		$headers .= 'NÚMERO DE DOCUMENTO TUTOR'. "\t";
		$headers .= 'TELÉFONO TUTOR'. "\t";
		$headers .= 'CORREO TUTOR'. "\t";
		$headers .= 'DEPARTAMENTO TUTOR'. "\t";
		$headers .= 'PROVINCIA TUTOR'. "\t";
		$headers .= 'DISTRITO TUTOR'. "\t";
		$headers .= 'DIRECCIÓN TUTOR'. "\t";

		$headers .= 'TIPO DE SERVICIO'. "\t";
		$headers .= 'MONTO DE RECLAMO'. "\t";
		$headers .= 'TIPO DE RECLAMO'. "\t";
		$headers .= 'ESTABLECIMIENTO'. "\t";
		$headers .= 'DESCRIPCIÓN DEL BIEN CONTRATADO'. "\t";
		$headers .= 'DETALLE DEL RECLAMO'. "\t";
		$headers .= 'PEDIDO DEL RECLAMO'. "\t";
		$headers .= 'RESPONSABLE'. "\t";
		$headers .= 'SUB ESTADO'. "\t";
		$headers .= 'ESTADO'. "\t";
		$headers .= 'FECHA REGISTRO'. "\t";
		$headers .= 'FECHA DE RESOLUCIÓN';


		if(isset($obj_registros)) {
			foreach($obj_registros as $obj_registro) {
				$typedocc = $obj_registro->typedoc_child == 0 ? "DNI":"CE";
				$typedoct = $obj_registro->typedoc_tutor == 0 ? "DNI":"CE";
				$typeserv = $obj_registro->typeservice == 0 ? "Producto":"Servicio";
				$addresss = $obj_registro->address_station;
				$claim_amount = $obj_registro->claim_amount;
				$typeservice_description = $obj_registro->typeservice_description;
				$complain_detail = $obj_registro->complain_detail;
				$complain_order = $obj_registro->complain_order;
				$complain = $obj_registro->complain == 0 ? 'Reclamo':'Queja';

				$line = "";
				$line .= $obj_registro->code."\t";
				$line .= $obj_registro->name_child." ".$obj_registro->lastnamefa_child." ".$obj_registro->lastnamema_child."\t";
				$line .= $typedocc."\t";
				$line .= $obj_registro->numberdoc_child."\t";
				$line .= $obj_registro->phone_child."\t";
				$line .= $obj_registro->email_child."\t";
				$line .= $this->obj_visitor->response_ubigeo($obj_registro->department_child)."\t";
				$line .= $this->obj_visitor->response_ubigeo($obj_registro->province_child)."\t";
				$line .= $this->obj_visitor->response_ubigeo($obj_registro->district_child)."\t";
				$line .= $obj_registro->address_child."\t";

				if($obj_registro->younger == 1){
					$line .= $obj_registro->name_tutor." ".$obj_registro->lastnamefa_tutor." ".$obj_registro->lastnamema_tutor."\t";
					$line .= $typedoct."\t";
					$line .= $obj_registro->numberdoc_tutor."\t";
					$line .= $obj_registro->phone_tutor."\t";
					$line .= $obj_registro->email_tutor."\t";
					$line .= $this->obj_visitor->response_ubigeo($obj_registro->department_tutor)."\t";
					$line .= $this->obj_visitor->response_ubigeo($obj_registro->province_tutor)."\t";
					$line .= $this->obj_visitor->response_ubigeo($obj_registro->district_tutor)."\t";
					$line .= $obj_registro->address_tutor."\t";
				}else{
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
					$line .= "-\t";
				}
				
				$line .= $typeserv."\t";
				$line .= $claim_amount."\t";
				$line .= $complain."\t";
				$line .= $addresss."\t";
				$line .= $typeservice_description."\t";
				$line .= $complain_detail."\t";
				$line .= $complain_order."\t";
				
				if($obj_registro->status == 1){
					$substatus = $obj_registro->sub_status == 1 ? 'Aprobado':'Rechazado';
					$line .= $obj_registro->user_name."\t";
					$line .= $substatus."\t";
					$line .= "Finalizado\t";
				}else{
					$line .= "-\t";
					$line .= "-\t";
					$line .= "Pendiente\t";
				}

				$line .= date("d/m/Y", strtotime($obj_registro->date_creation))."\t";
				
				$line .= $obj_registro->date_resolution."\t";

				$data .= trim($line)."\n";
			}  
		}
		
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename.xls");
		echo mb_convert_encoding("$headers\n$data",'utf-16','utf-8');
	}

	public function filterexport() {
		$filter = array();
		
		if($_POST["station_address"] != ''){
			$filter["address_station"] = $_POST["station_address"];
		}
		if($_POST["complain_type"] != ''){
			$filter["typeservice"] = $_POST["complain_type"];
		}
		if($_POST["complain_status"] != ''){
			$filter["status"] = $_POST["complain_status"];
		}
		if($_POST["complain_substatus"] != ''){
			$filter["sub_status"] = $_POST["complain_substatus"];
		}
		if($_POST["complain_executive"] != ''){
			$filter["user_id"] = $_POST["complain_executive"];
		}
		
		$obj_registros = $this->obj_visitor->filter_report($filter);
		$headers = ''; // just creating the var for field headers to append to below
		$data = ''; // just creating the var for field data to append to below
		$filename = 'reporte';
		
		$headers .= 'NOMBRES Y APELLIDOS'. "\t";
		$headers .= 'CORREO'. "\t";
		$headers .= 'TIPO DE SERVICIO'. "\t";
		$headers .= 'ESTABLECIMIENTO'. "\t";
		$headers .= 'FECHA REGISTRO';


		if(isset($obj_registros)) {
			foreach($obj_registros as $obj_registro) {
				$typeserv = $obj_registro->typeservice == 0 ? "Producto":"Servicio";
				$addresss = $this->obj_visitor->response_station($obj_registro->address_station);

				$line = "";
				$line .= $obj_registro->name_child." ".$obj_registro->lastnamefa_child." ".$obj_registro->lastnamema_child."\t";
				$line .= $obj_registro->email_child."\t";
				$line .= $typeserv."\t";
				$line .= $addresss."\t";
				$line .= date("d/m/Y", strtotime($obj_registro->date_creation))."\t";
				$data .= trim($line)."\n";
			} 
		}
		
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename.xls");
		echo mb_convert_encoding("$headers\n$data",'utf-16','utf-8');
	}

	public function users() {

		if (!$this->validUser()) {
			$this->load->view('error');
		} else {
			$this->db->select('user_id');
			$this->db->from('user');
			$this->db->where('status <> 2');
	
			$query = $this->db->get('');
	
			$start = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

			$opciones = array();
			$opciones['per_page'] = 20;
			$opciones['base_url'] = site_url().'/dashboard/'.$this->uri->segment(2);
			$opciones['total_rows'] = $query->num_rows();
			$opciones['uri_segment'] = 3;
			$opciones['first_tag_open'] = '<li>';
			$opciones['first_tag_close'] = '</li>';
			$opciones['prev_tag_open'] = '<li>';
			$opciones['prev_tag_close'] = '</li>';
			$opciones['num_tag_open'] = '<li>';
			$opciones['num_tag_close'] = '</li>';
			$opciones['cur_tag_open'] = '<li class="active"><a>';
			$opciones['cur_tag_close'] = '</li></a>';
			$opciones['next_tag_open'] = '<li>';
			$opciones['next_tag_close'] = '</li>';
			$opciones['last_tag_open'] = '<li>';
			$opciones['last_tag_close'] = '</li>';
	
			$this->pagination->initialize($opciones);
		
			$obj_visitors = $this->obj_visitor->users_list($start, $opciones['per_page'], 1);
	
			$this->cms_tmp_master->set('obj_visitors',$obj_visitors);
			$this->cms_tmp_master->set('obj_paginacion',$this->pagination->create_links());
			$this->cms_tmp_master->render('dashboard/users');
		}
	}

	function delete_user() {
		$id = $_POST["id"];

		if($this->obj_visitor->delete_users($id)){
			$data["message"] = "Se elimino el usuario correctamente";
    	$data["status"] = true;
		}else{
			$data["message"] = "No se pudo eliminar el usuario";
    	$data["status"] = false;
		}
		echo json_encode($data);
	}

	public function complaint_response() {
		$id = $this->input->post('id');

		// $data = $this->obj_visitor->complain_detail($id);
		$dataObjs = $this->obj_visitor->complain_detail($id);

		$data = array();

		foreach ($dataObjs as $dataObj){
			$dataRes['id'] = $dataObj->id;
			$dataRes['code'] = $dataObj->code;
			$dataRes['typedoc_child'] = $dataObj->typedoc_child;
			$dataRes['numberdoc_child'] = $dataObj->numberdoc_child;
			$dataRes['namechild_full'] = $dataObj->name_child.' '.$dataObj->lastnamefa_child.' '.$dataObj->lastnamema_child;
			$dataRes['phone_child'] = $dataObj->phone_child;
			$dataRes['department_child'] = $dataObj->department_child;
			$dataRes['province_child'] = $dataObj->province_child;
			$dataRes['district_child'] = $dataObj->district_child;
			$dataRes['address_child'] = $dataObj->address_child;
			$dataRes['email_child'] = $dataObj->email_child;
			$dataRes['younger'] = $dataObj->younger;
			$dataRes['typedoc_tutor'] = $dataObj->typedoc_tutor;
			$dataRes['numberdoc_tutor'] = $dataObj->numberdoc_tutor;
			$dataRes['lastnamefa_tutor'] = $dataObj->lastnamefa_tutor;
			$dataRes['lastnamema_tutor'] = $dataObj->lastnamema_tutor;
			$dataRes['name_tutor'] = $dataObj->name_tutor;
			$dataRes['phone_tutor'] = $dataObj->phone_tutor;
			$dataRes['department_tutor'] = $dataObj->department_tutor;
			$dataRes['province_tutor'] = $dataObj->province_tutor;
			$dataRes['district_tutor'] = $dataObj->district_tutor;
			$dataRes['address_tutor'] = $dataObj->address_tutor;
			$dataRes['email_tutor'] = $dataObj->email_tutor;
			$dataRes['typeservice'] = $dataObj->typeservice == 0 ? 'Producto':'Servicio';
			$dataRes['claim_amount'] = $dataObj->claim_amount;
			
			$dataSede = $this->obj_visitor->sedes_name($dataObj->address_station);
			$dataRes['address_station'] = $dataSede;

			$dataRes['typeservice_description'] = $dataObj->typeservice_description;
			$dataRes['complain'] = $dataObj->complain == 0 ? 'Reclamo':'Queja';
			$dataRes['complain_detail'] = $dataObj->complain_detail;
			$dataRes['complain_order'] = $dataObj->complain_order;

			// $dataRes['notify'] = $dataObj->notify;

			switch ($dataObj->notify) {
				case 1:
					$dataRes['notify'] = '<span class="label label-primary">Email</span>';
					break;
				case 2:
					$dataRes['notify'] = '<span class="label label-primary">Domicilio</span>';
					break;
				default:
					$dataRes['notify'] = '<span class="label label-primary">Email</span>';
					break;
			}

			$dataRes['notify_number'] = $dataObj->notify;

			$dataRes['address_notify'] = $dataObj->address_notify != '' ? $dataObj->address_notify:$dataObj->email_child;
			$dataRes['institution'] = $dataObj->institution;
			// $dataRes['date_creation'] = $dataObj->date_creation;
			$dataRes['date_creation'] = date_format(date_create($dataObj->date_creation), 'd-m-Y');

			switch ($dataObj->sub_status) {
				case 1:
					$dataRes['sub_status'] = '<span class="label label-success">Aprobado</span>';
					break;
				case 2:
					$dataRes['sub_status'] = '<span class="label label-danger">Rechazado</span>';
					break;
				default:
					$dataRes['sub_status'] = '<span>-</span>';
					break;
			}

			switch ($dataObj->status) {
				case 0:
					$dataRes['status'] = '<span class="label label-info">Pendiente</span>';
					$dataRes['status_number'] = 0;
					break;
				case 1:
					$dataRes['status'] = '<span class="label label-primary">Finalizado</span>';
					$dataRes['status_number'] = 1;
					break;
				default:
					$dataRes['status'] = '<span>-</span>';
					break;
			}

			if($dataObj->user_id){
				$dataName = $this->obj_visitor->users_name($dataObj->user_id);
			}else{
				$dataName = '-';
			}

			$dataRes['user_id'] = $dataName;

			$dataRes['date_resolution'] = date_format(date_create($dataObj->date_resolution), 'd-m-Y');
			$dataRes['url_pdf'] = $dataObj->url_pdf;
			$dataRes['complaint_mail_cco'] = $dataObj->complaint_mail_cco == '' ? '':$dataObj->complaint_mail_cco;

			$fileResult = $dataObj->complaint_mail_files;
			$files = explode( ',' , $fileResult );

			if($files){
				$dataRes['complaint_mail_files'] =  $files;
			}else{
				$dataRes['complaint_mail_files'] =  '';
			}

			$dataRes['complaint_mail_text'] = $dataObj->complaint_mail_text;

			// $dataRes['complaint_mail_files'] = $dataObj->complaint_mail_files;

			$data[] = $dataRes;
		}

		$data["status"] = true;
		$data["message"] = 'Se obtuvo la data correctamente';

		echo json_encode($data);
	}

	function complaint_view() {
		$id = $_POST["id"];

		$dataObjs = $this->obj_visitor->complain_detail($id);

		$data = array();

		foreach ($dataObjs as $dataObj){
			$dataRes['id'] = $dataObj->id;
			$dataRes['code'] = $dataObj->code;
			$dataRes['typedoc_child'] = $dataObj->typedoc_child;
			$dataRes['numberdoc_child'] = $dataObj->numberdoc_child;
			$dataRes['namechild_full'] = $dataObj->name_child.' '.$dataObj->lastnamefa_child.' '.$dataObj->lastnamema_child;
			$dataRes['phone_child'] = $dataObj->phone_child;
			$dataRes['department_child'] = $dataObj->department_child;
			$dataRes['province_child'] = $dataObj->province_child;
			$dataRes['district_child'] = $dataObj->district_child;
			$dataRes['address_child'] = $dataObj->address_child;
			$dataRes['email_child'] = $dataObj->email_child;
			$dataRes['younger'] = $dataObj->younger;
			$dataRes['typedoc_tutor'] = $dataObj->typedoc_tutor;
			$dataRes['numberdoc_tutor'] = $dataObj->numberdoc_tutor;
			$dataRes['lastnamefa_tutor'] = $dataObj->lastnamefa_tutor;
			$dataRes['lastnamema_tutor'] = $dataObj->lastnamema_tutor;
			$dataRes['name_tutor'] = $dataObj->name_tutor;
			$dataRes['phone_tutor'] = $dataObj->phone_tutor;
			$dataRes['department_tutor'] = $dataObj->department_tutor;
			$dataRes['province_tutor'] = $dataObj->province_tutor;
			$dataRes['district_tutor'] = $dataObj->district_tutor;
			$dataRes['address_tutor'] = $dataObj->address_tutor;
			$dataRes['email_tutor'] = $dataObj->email_tutor;
			$dataRes['typeservice'] = $dataObj->typeservice == 0 ? 'Producto':'Servicio';
			$dataRes['claim_amount'] = $dataObj->claim_amount;
			
			$dataSede = $this->obj_visitor->sedes_name($dataObj->address_station);
			$dataRes['address_station'] = $dataSede;

			$dataRes['typeservice_description'] = $dataObj->typeservice_description;
			$dataRes['complain'] = $dataObj->complain == 0 ? 'Reclamo':'Queja';
			$dataRes['complain_detail'] = $dataObj->complain_detail;
			$dataRes['complain_order'] = $dataObj->complain_order;
			$dataRes['notify'] = $dataObj->notify;
			$dataRes['address_notify'] = $dataObj->address_notify != '' ? $dataObj->address_notify:$dataObj->email_child;
			$dataRes['institution'] = $dataObj->institution;
			$dataRes['date_creation'] = $dataObj->date_creation;

			switch ($dataObj->sub_status) {
				case 1:
					$dataRes['sub_status'] = '<span class="label label-success">Aprobado</span>';
					break;
				case 2:
					$dataRes['sub_status'] = '<span class="label label-danger">Rechazado</span>';
					break;
				default:
					$dataRes['sub_status'] = '<span>-</span>';
					break;
			}

			switch ($dataObj->status) {
				case 0:
					$dataRes['status'] = '<span class="label label-info">Pendiente</span>';
					$dataRes['status_number'] = 0;
					break;
				case 1:
					$dataRes['status'] = '<span class="label label-primary">Finalizado</span>';
					$dataRes['status_number'] = 1;
					break;
				default:
					$dataRes['status'] = '<span>-</span>';
					break;
			}

			if($dataObj->user_id){
				$dataName = $this->obj_visitor->users_name($dataObj->user_id);
			}else{
				$dataName = '-';
			}

			$dataRes['user_id'] = $dataName;

			$dataRes['date_resolution'] = date_format(date_create($dataObj->date_resolution), 'd-m-Y');
			$dataRes['url_pdf'] = $dataObj->url_pdf;
			$dataRes['complaint_mail_cco'] = $dataObj->complaint_mail_cco == '' ? '-':$dataObj->complaint_mail_cco;

			$fileResult = $dataObj->complaint_mail_files;
			$files = explode( ',' , $fileResult );

			$dataRes['complaint_mail_files'] = $files;

			$dataRes['complaint_mail_text'] = $dataObj->complaint_mail_text;

			// $dataRes['complaint_mail_files'] = $dataObj->complaint_mail_files;

			$data[] = $dataRes;
		}

		$data["status"] = true;

		echo json_encode($data);
	}

	public function removeFilesSave(){
		$id = $this->input->post('id');
		$index = $this->input->post('index');

		$dataObjs = $this->obj_visitor->complain_detail($id);

		foreach ($dataObjs as $dataObj){
			$fileResult = $dataObj->complaint_mail_files;
			$file = explode( ',' , $fileResult );
		}

		unset($file[$index]);

		$fileString = implode(",", $file);

		$data["file"] = $this->obj_visitor->remove_files_save($fileString,$id);
		$data["status"] = true;
		$data["message"] = "El archivo fue eliminado con éxito";

		echo json_encode($data);
	}


	function status_complaint() {
		$id = $_POST["id"];
		$idUser = $_POST["id_user"];
		$action = $_POST["action"];
		$nameUser = $_POST["user_name"];

		if($this->obj_visitor->change_status_complaint($id,$idUser,$action,$nameUser)){
			$data["message"] = "Se cambio el estado";
    	$data["status"] = true;
		}else{
			$data["message"] = "No se cambio el estado";
    	$data["status"] = false;
		}
		echo json_encode($data);
	}

	function userStatus() {
		$id = $_POST["id"];
		$status = $_POST["status"];

		if($this->obj_visitor->change_status($id,$status)){
			$data["message"] = "Se cambio el estado";
    	$data["status"] = true;
			if($status == 1){
				$data["value"] = 'Inactivo';
				$data["value_num"] = 0;
			}else {
				$data["value"] = 'Activo';
				$data["value_num"] = 1;
			}
		}else{
			$data["message"] = "No se cambio el estado";
    	$data["status"] = false;
		}
		echo json_encode($data);
	}

	public function update_status() {

		$data['print'] = "false";

		if( $this->input->is_ajax_request() ) {
			$visitor_id = $this->input->post('id');
			$status = $this->input->post('status');

			if (
				! @preg_match('/^[0-9]+$/', $visitor_id) ||
				! @preg_match('/^[0-9]+$/', $status)
				)
			{
				echo json_encode($data);

				exit();
			}

			$data_save['visible'] = $status;

			if ( $this->obj_visitor->update($visitor_id, $data_save) )
			{
				$data['print'] = "true";
			}
			
			echo json_encode($data);

			exit();
		}
	}

	public function sendMail($email,$content){
		//cargamos la libreria email de ci
		$this->load->library("email");

		//configuracion para gmail
		$configGmail = array(
			'protocol' => 'smtp',
			// 'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_host' => 'ssl://email-smtp.us-east-1.amazonaws.com',
			'smtp_port' => 465,
			// 'smtp_user' => 'develop@solera.pe',
			'smtp_user' => 'AKIAJHSEEWY2AI4Q2Z4A',
			'smtp_pass' => 'Aj9c74aMxOYbme5Rq4tqIfGSR0k63smClkrHcML+M3FB',
			// 'smtp_pass' => '**empoweryourself**',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
		);

		//cargamos la configuración para enviar con gmail
		$this->email->initialize($configGmail);
		$this->email->from('no-reply@solera.pe', 'UDEP');
		$this->email->to($email);
		$this->email->subject('UDEP - Contraseña de accesos');
		$this->email->message($content);
		$this->email->set_newline("\r\n");
		$this->email->send();

		//con esto podemos ver el resultado
		// var_dump($this->email->print_debugger());
	}
	
	public function graphic(){
		$this->cms_tmp_master->render('dashboard/graphic');
	}

	public function mail_response(){
		$this->load->view('mail_response');
	}

	public function complaintAddFilesOrden($correlativo){
		$this->complaintAddFiles($correlativo, false);
	}

	public function complaintAddFiles($id, $isID = true){
		if ( count($_FILES) > 0 ) {
			$config['upload_path']   = './uploads/attachments/';
			$config['allowed_types'] = 'gif|jpg|png|pdf';
			$config['max_size']      = 1024*5;

			$filenameorigin = explode(".", $_FILES["file"]['name']);
			$filenameExtRemove = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_FILES['file']['name']);
			$filenameSubstr = substr(slug($filenameExtRemove), 0, 15);
			
			$new_name = date("YmdHis").'_'.$filenameSubstr.'.'.end($filenameorigin);
			$config['file_name'] = $new_name;
			
			$fileName = $_FILES['file']['name'];
			$fileSize = $_FILES['file']['size'];
			$fileSizeKb = $fileSize/1024;

			$this->load->library('upload', $config);
			// $this->upload->do_upload('file');

			if($this->upload->do_upload('file')){
				if($this->obj_visitor->complaint_mail_files($id, $new_name)){
					$data = array(
						'nombre' => $new_name,
						'size' => $fileSize,
						'message' => 'Imagen subida correctamente',
						'size_status' => false,
						'id' => $id,
						'error' => false
					);
				}else{
					$data = array(
						'message' => 'La imagen no pudo subir',
						'size_status' => false,
						'error' => true
					);
				}
			}

			if($config['max_size'] >= $fileSizeKb){
				$data['size_status'] = true;
			}
			echo json_encode($data);
		}
		exit;
	}

	public function complaint_form(){
		$id = $this->input->post('id');
		$action = $this->input->post('action');
		$message = $this->input->post('message');
		$cco = $this->input->post('cco');
		$userid = $this->input->post('userid');
		$email = $this->input->post('emailchild');
		$notify = $this->input->post('notify');

		if($this->obj_visitor->complaint_form_rejected($id,$message,$cco,$action,$userid)){
			if($action == 0){
				$data['message'] = 'La respuesta fue guardada correctamente';
			}else{
				$datamail['message'] = $message;
				$datamail['code'] = $this->input->post('code');
				$fileResult = $this->obj_visitor->complaint_attach($id);

				$files = explode( ',' , $fileResult );

				$ccoArray = explode( ',' , $cco );
				
				$content = $this->load->view("mail_response", $datamail, true);
				
				if($notify == 1){
					$tutorEmail = $this->obj_visitor->response_email_tutor($id);

					array_push($ccoArray, $email);
					array_push($ccoArray, $tutorEmail);
					$this->sendMailResponse($content,$ccoArray,$files);
					$data['message'] = 'La respuesta fue enviada';
				}
				if($notify == 2){
					$data['message'] = 'La respuesta se guardo correctamente. La respuesta será enviada a domicilio.';
				}

			}
			$data['status'] = true;
		}else{
			$data['message'] = 'El mensaje no pudo salir';
			$data['status'] = false;
		}

		echo json_encode($data);
	}

	public function mailResponse(){
		$this->load->view("mail_response");
	}

	public function sendMailResponse($content,$ccoArray,$files){
		// cargamos la libreria email de ci
		$this->load->library("email");

		// configuracion para gmail
		$configGmail = array(
			'protocol' => 'smtp',
			// 'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_host' => 'ssl://email-smtp.us-east-1.amazonaws.com',
			'smtp_port' => 465,
			// 'smtp_user' => 'develop@solera.pe',
			'smtp_user' => 'AKIAJHSEEWY2AI4Q2Z4A',
			'smtp_pass' => 'Aj9c74aMxOYbme5Rq4tqIfGSR0k63smClkrHcML+M3FB',
			// 'smtp_pass' => '**empoweryourself**',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
		);

		// cargamos la configuración para enviar con gmail
		$this->email->initialize($configGmail);
		$this->email->from('no-reply@solera.pe', 'UDEP');

		$this->email->to($ccoArray);
		
		$this->email->subject('UDEP - Respuesta a su reclamo');
		$this->email->message($content);

		if($files != array("")){
			foreach ($files as $row){
				$this->email->attach(site_url().'uploads/attachments/'.$row);
			}
		}

		$this->email->send();

		// con esto podemos ver el resultado
		// var_dump($this->email->print_debugger());
	}
}