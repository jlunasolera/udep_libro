$.fn.extend({
	jlModal: function(options){
		var modalBox = $(this).attr('data-jlmodal-id'),
				modalExi = $('.jlmodal-close');

		defaults = {
			color: '#000',
			title: 'title'
		};

		var options = $.extend({}, defaults, options);

		return this.each(function(){

			$(this).click(function(){
				$('#' + modalBox).find('.jlmodal-content').removeClass('jmodal-hide')
				$('#' + modalBox).find('.jlmodal-content').addClass('jmodal-show')
				$('#' + modalBox).show()
				$('body').css('overflow','hidden')
			});
			modalExi.click(function(){
				$('#' + modalBox).find('.jlmodal-content').removeClass('jmodal-show')
				$('#' + modalBox).find('.jlmodal-content').addClass('jmodal-hide')
				setTimeout(function(){ $('#' + modalBox).hide() }, 250);
				$('body').css('overflow','visible')
			});
			$(window).click(function(e) {
				if(e.target.className == 'jlmodal') {
					$('#' + modalBox).find('.jlmodal-content').removeClass('jmodal-show')
					$('#' + modalBox).find('.jlmodal-content').addClass('jmodal-hide')
					setTimeout(function(){ $('#' + modalBox).hide() }, 250);
					$('body').css('overflow','visible')
				}
			});


		});

	}
})
