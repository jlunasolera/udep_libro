<section class="content-header">
	<h1>Usuarios</h1>
  <div class="breadcrumb">
    <button type="button" class="tipo btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-md"><span class="btn-label icon fa fa-plus" ></span>&nbsp;&nbsp;Nuevo</button>
  </div>
</section>

<section class="content">
	<div class="row">
    <div class="col-sm-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Lista de usuarios</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-bordered" id="tableUserlist">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Perfil</th>
                <th>Estado</th>
                <th>Reset Clave</th>
                <th>Acción</th>
              </tr>
            </thead>
            <tbody>
              <?php if(isset($obj_visitors) && count($obj_visitors) > 0){
              foreach($obj_visitors as $obj_visitor){?>
                <tr class="gradeA odd" id="tr<?php echo $obj_visitor->user_id ?>">
                  <input type="hidden" value="<?php echo $obj_visitor->user_id ?>">
                  <td><?php echo $obj_visitor->name.' '.$obj_visitor->lastname ?></td>
                  <td><?php echo $obj_visitor->email ?></td>
                  <td><?php echo $obj_visitor->rol == 0 ? 'Ejecutivo':'Admin' ?></td>
                  <td>
                    <?php switch ($obj_visitor->status) {
                      case 0:
                        echo '<button type="button" class="btn btn-default btn-sm userStatus" data-status="0"><span class="statusValue">Inactivo</span></button>';
                        break;
                      case 1:
                        echo '<button type="button" class="btn btn-default btn-sm userStatus" data-status="1"><span class="statusValue">Activo</span></button>';
                        break;
                    } ?>
                  </td>
                  <td>
                    <button type="button" class="btn btn-sm btn-warning userResetPass">
                      <span class="btn-label icon fa fa-refresh" aria-hidden="true"></span> Reiniciar
                    </button>
                  </td>
                  <td>
                    <button type="button" class="btn btn-sm btn-danger userDelete">
                      <span class="btn-label icon fa fa-trash-o" aria-hidden="true"></span>
                    </button>
                  </td>

                </tr>
              <?php }} ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <?php if(isset($obj_paginacion) && empty($obj_paginacion) != 1) {?>
          <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right"><?php echo $obj_paginacion ?></ul>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>

<!-- modal nuevo -->

<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form id="newRegister">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Agregar usuario</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" class="form-control" id="newuser_name" placeholder="Nombre" name="newuser_name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="newuser_lastname" placeholder="Apellido" name="newuser_lastname">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="newuser_email" placeholder="E-mail" name="newuser_email">
          </div>
        
          <div class="form-group">
            <select name="newuser_typerol" id="newuser_typerol" class="form-control">
              <option value="">- Seleccionar perfil -</option>
              <option value="0">Ejecutivo</option>
              <option value="1">Administrador</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="btnNewRegister" class="btn btn-primary">Registrar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script src="<?php echo site_url() ?>static/cms/js/jquery.validate.min.js"></script>
<script src="<?php echo site_url() ?>static/cms/js/users.js"></script>