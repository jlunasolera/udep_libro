<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Acceso CMS Admin</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&amp;subset=latin" rel="stylesheet" type="text/css">
	<!-- Pixel Admin's stylesheets -->
	<link href="<?php echo site_url()?>static/cms/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url()?>static/cms/css/pixel-admin.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url()?>static/cms/css/pages.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url()?>static/cms/css/rtl.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url()?>static/cms/css/themes.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo site_url()?>static/cms/css/nextperience.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]><script src="<?php echo site_url()?>static/cms/js/ie.min.js"></script><![endif]-->
</head>
<body class="theme-dust page-signin">
<div id="page-signin-bg">
	<img src="<?php echo site_url()?>static/cms/images/signin-bg-9.jpg" alt="">
</div>
<div class="signin-container">
	<div class="signin-form">
		<div id="logo-nextperience">
			 <img src="<?php echo site_url()?>static/cms/images/logo-nextperience.png"> 
		</div>
		<form action="<?php echo site_url()?>dashboard" method="post" id="signin-form_id">
			<div class="signin-text">
				<span>Ingresa a tu cuenta</span>
			</div>
			<div class="form-group w-icon">
				<input type="text" name="email" id="email" class="form-control input-lg" placeholder="Correo">
				<span class="fa fa-user signin-form-icon"></span>
			</div>
			<div class="form-group w-icon">
				<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Contrase&ntilde;a">
				<span class="fa fa-lock signin-form-icon"></span>
			</div>
			<div class="form-actions">
				<input type="submit" value="ENTRAR" class="signin-btn bg-primary">
			</div>
		</form>
		<div class="signin-with">
			<p class="text-danger"><?php echo isset($error)?$error:'';?></p>
		</div>
	</div>
</div>

<!--[if !IE]> -->
<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo site_url()?>static/cms/js/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->

</body>
<!-- Mirrored from infinite-woodland-5276.herokuapp.com/pages-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jun 2014 17:47:37 GMT -->
</html>
