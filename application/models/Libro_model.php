<?php
class Libro_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function list_sedes(){

    $this->db->select('*');
    $this->db->from('sedes');
    $this->db->order_by('name ASC');

    $query_sedes = $this->db->get();

    return $query_sedes->result();
  }

  public function select_users(){

    $this->db->select('*');
    $this->db->from('user');
    $this->db->order_by('name ASC');

    $query_users = $this->db->get();

    return $query_users->result();
  }

  public function select_reclamo($order){

    $this->db->select('*');
    $this->db->from('reclamo');
    $this->db->where('code', $order);

    $query_result = $this->db->get();

    return $query_result->result();
  }

}