;(function ($) {
	$('.complaint-response').on('click', function(e){
		var $id = $(this).attr('data-id');
		complaintResponse($id);
	})

	// CKEDITOR.config.toolbar_MyCustomToolbar = [ [ 'Underline','Subscript','Superscript','NumberedList','BulletedList','Bold','Italic' ] ];
	// ckeditor ini
	CKEDITOR.editorConfig = function (config) {
		config.language = 'es';
		config.uiColor = '#F7B42C';
		config.height = 300;
		config.toolbarCanCollapse = true;
	}

	CKEDITOR.replace('complaintResponseMail');
	// ckeditor end

	// dropzone ini
	var previewNode = document.querySelector("#template");
	previewNode.id = "";
	var previewTemplate = previewNode.parentNode.innerHTML;
	previewNode.parentNode.removeChild(previewNode);

	if ( $(".filesDropzoneDesign").length > 0 ) {
		var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
			url: site+"complaint_add_files/post", // Set the url
			thumbnailWidth: 80,
			thumbnailHeight: 80,
			parallelUploads: 20,
			acceptedFiles: ".gif,.jpg,.png,.pdf",
			previewTemplate: previewTemplate,
			autoQueue: true, // Make sure the files aren't queued until manually added
			// previewsContainer: "#previews", // Define the container to display the previews
			clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
			init: function() {
				this.on("success", function(file, responseText) {
					PNotify.prototype.options.styling = "bootstrap3";

					var $response = JSON.parse(responseText);

					if($response.error){
						new PNotify({
							title: 'Error',
							text: $response.message,
							type: 'error'
						});
					}else{
						new PNotify({
							title: 'Correcto',
							text: $response.message,
							type: 'success'
						});
					}

				});

				this.on("processing", function(file) {
					var $correlativo = $('.filesDropzoneDesign').attr('data-id');
					this.options.url = site+"complaint_add_files_orden/"+$correlativo;
				});
			}
		});

		// Hide the total progress bar when nothing's uploading anymore
		myDropzone.on("queuecomplete", function(progress) {
			var $idComplaint = $('.filesDropzoneDesign').attr('data-id');
			complaintFile($idComplaint);
		});
	}

	// dropzone end

	$('.complaintAction').on('click', function(e){
		$('.complaintAction').prop('disabled', true);

		$action = $(this).attr('data-action');
		$id = $('input[name="complaint_id"]').val();

		switch($action){
			case '0':
				responseEmailStatus(0,$id);
				break;
			case '1':
				responseEmailStatus(1,$id);
				break;
			case '2':
				responseEmailStatus(2,$id);
				break;
		}
		
	})

	$('#modalComplaintResponse').on('hidden.bs.modal', function () {
		$('#previews-save').html('');
		$('#copyhidden').val('');
		CKEDITOR.instances.complaintResponseMail.setData('');
	})

})(jQuery);

function complaintFile($id){
	$.ajax({
		url: site + 'dashboard/visitors/complaint_response',
		type:'post',
		dataType: "json",
		data: {
      "id": $id,
		},
		success: function(data){
			if (data.status){
				var $data = data[0];

				$filesArray = $data.complaint_mail_files;
				
				if($filesArray != ''){
					$('#previews-save').html("");
	
					$filesArray.forEach( function(valor, indice, array) {
						var $html;
						
						$html = `<div id="" class="file-row">
						<table class="table table-dropzone">
						<tbody>
						<tr>
						<td>
						<p class="name">`+ valor +`</p>
						</td>
						<td>
						<button class="btn btn-danger delete delete-file" onclick="removeFileSave(`+ $id +`,`+ indice +`)">
						<i class="glyphicon glyphicon-trash"></i>
						</button>
						</td>
						</tr>
						</tbody>
						</table>
						</div>`;
						
						$('#previews-save').append($html);
					});
				}

			} else {
				console.log("no se cargaron los datos");
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}

function complaintResponse($id){
	$.ajax({
		url: site + 'dashboard/visitors/complaint_response',
		type:'post',
		dataType: "json",
		data: {
      "id": $id,
		},
		success: function(data){
			if (data.status){
				var $data = data[0];

				$('.filesDropzoneDesign').attr('data-id',$data.id);
				$('input[name="complaint_id"]').val($data.id);
				$('input[name="complaint_code"]').val($data.code);
				$('input[name="complaint_emailchild"]').val($data.email_child);
				$('#complaintResponse_code').text($data.code);
				$('#complaintResponse_name').find('.responseComplaintId').text($data.namechild_full);
				$('#complaintResponse_email').find('.responseComplaintId').text($data.email_child);
				$('#complaintResponse_dateregister').find('.responseComplaintId').text($data.date_creation);

				$('#complaintResponse_typesend').find('.responseComplaintId').html($data.notify);
				$('input[name="complaint_typesend"]').val($data.notify_number);

				if($data.complaint_mail_cco){
					$('#copyhidden').val($data.complaint_mail_cco);
				}

				if($data.complaint_mail_text){
					CKEDITOR.instances['complaintResponseMail'].destroy(true);
					$('#complaintResponseMail').text($data.complaint_mail_text);
					$('#complaintResponseMailHtml').html($data.complaint_mail_text);
					CKEDITOR.replace('complaintResponseMail');
				}

				
				if($data.url_pdf){
					$('#complaintResponse_filepdf').find('a').attr('href',$url_site+$data.url_pdf);
					$('#complaintResponse_filepdf').find('a').text($data.url_pdf);
				}
				
				$filesArray = $data.complaint_mail_files;
				
				if($filesArray != ''){
					$filesArray.forEach( function(valor, indice, array) {
						var $html;
						
						$html = `<div id="" class="file-row">
						<table class="table table-dropzone">
						<tbody>
						<tr>
						<td>
						<p class="name">`+ valor +`</p>
						</td>
						<td>
						<button class="btn btn-danger delete delete-file" onclick="removeFileSave(`+ $id +`,`+ indice +`)">
						<i class="glyphicon glyphicon-trash"></i>
						</button>
						</td>
						</tr>
						</tbody>
						</table>
						</div>`;
						
						$('#previews-save').append($html);
					});
				}

				if($data.status_number == 1){
					$('.modal-footer').hide();
					$('#btnFilesAdd').hide();
					$('#copyhidden').prop('disabled', true);
					$('.delete-file').remove();
					$('#complaintResponseMailBox').hide();
					$('#complaintResponseMailHtml').show();
				}else{
					$('.modal-footer').show();
					$('#btnFilesAdd').show();
					$('#copyhidden').prop('disabled', false);
					$('#complaintResponseMailBox').show();
					$('#complaintResponseMailHtml').hide();
					// CKEDITOR.instances['complaintResponseMail'].setReadOnly(false);
				}
				
        $('#modalComplaintResponse').modal('show');
			} else {
				console.log("no se cargaron los datos");
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}

function responseEmailStatus($action,$id){
	$responseMail = CKEDITOR.instances.complaintResponseMail.getData();
	$emailcco = $('#copyhidden').val();
	$userId = $('input[name="complaint_userid"]').val();
	$complaintCode = $('input[name="complaint_code"]').val();
	$userEmailchild = $('input[name="complaint_emailchild"]').val();
	$notifyId = $('input[name="complaint_typesend"]').val();

	$.ajax({
		url: site + 'dashboard/respuesta_reclamo',
		type:'post',
		dataType: 'json',
		data: {
			'id': $id,
			'action': $action,
			'message': $responseMail,
			'cco': $emailcco,
			'userid': $userId,
			'emailchild': $userEmailchild,
			'code': $complaintCode,
			'notify': $notifyId
		},
		success: function(data){
			PNotify.prototype.options.styling = "bootstrap3";
			if (data.status){
				new PNotify({
					title: 'Correcto',
					text: data.message,
					type: 'success'
				});

				$('.complaintAction').prop('disabled', false);

				setTimeout(function(){
					window.location = site + 'dashboard/index';
				}, 3000);

				$('#modalComplaintResponse').modal('hide');
			} else {
				new PNotify({
					title: 'Error',
					text: data.message,
					type: 'error'
				});

				$('.complaintAction').prop('disabled', false);
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}

function removeFileSave($id,$index){
	$('.delete-file').prop('disabled',true);
	$.ajax({
		url: site + 'dashboard/remover_archivos',
		type:'post',
		dataType: 'json',
		data: {
			'id': $id,
			'index': $index
		},
		success: function(data){
			PNotify.prototype.options.styling = "bootstrap3";
			if (data.status){
				$('.delete-file').prop('disabled',false);
				new PNotify({
					title: 'Correcto',
					text: data.message,
					type: 'success'
				});

				var $filesArray = data.file;
				

				if($filesArray != ''){

					$('#previews-save').html('');

					var $html;

					$filesArray.forEach( function(valor, indice, array) {
						// $html = `<li>
						// 					<a href="`+ site_url +`uploads/attachments/`+ valor +`" target="_blank">`+ valor +`</a>
						// 				</li>`;
						$html = `<div id="" class="file-row">
											<table class="table table-dropzone">
												<tbody>
													<tr>
														<td>
															<p class="name">`+ valor +`</p>
														</td>
														<td>
															<button class="btn btn-danger delete delete-file" onclick="removeFileSave(`+ $id +`,`+ indice +`)">
																<i class="glyphicon glyphicon-trash"></i>
															</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>`;

						$('#previews-save').append($html);
					});
				} else {
					$('#previews-save').html('');
				}

			} else {
				new PNotify({
					title: 'Error',
					text: data.message,
					type: 'error'
				});
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}