var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    plumber = require('gulp-plumber'),
    pug = require('gulp-pug'),
    minifyCSS = require('gulp-minify-css'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    imageop = require('gulp-image-optimization');

var config = {
  styles:{
    main: './static/web/src/stylus/main.styl',
    watch: './static/web/src/stylus/**/*.styl',
    output: './static/web/dist/css'
  },
  scripts:{
    main: './static/web/src/js/main.js',
    watch: './static/web/src/js/**/*.js',
    output: './static/web/dist/js'
  },
  html: {
    main: './static/web/src/pug/*.pug',
    watch: './static/web/src/pug/**/*.pug',
    output: './static/web/dist'
    // output: './application/views'
  },
  images: {
    watch: ['./static/web/src/images/**/*.jpg', './static/web/src/images/**/*.png'],
    output: './static/web/dist/images'
  },
  fonts: {
    main: './static/web/src/fonts/**/*',
    output: './static/web/dist/fonts'
  }
};

gulp.task('server', function() {
  gulp.src('./static/web/dist')
    .pipe(webserver({
      host: '0.0.0.0',
      port: 8080,
      livereload: true
    }));
});

gulp.task('build:css', function() {
  gulp.src(config.styles.main)
    .pipe(plumber())
    .pipe(stylus({
      use: nib(),
      'include css': true
    }))
    .pipe(rename('main.min.css'))
		// .pipe(minifyCSS())
    .pipe(gulp.dest(config.styles.output));
});

gulp.task('build:js', function () {
  return browserify(config.scripts.main)
    .bundle()
    .on('error', function (err) {
      console.log(err.message)
      this.emit('end')
    })
    .pipe(source('bundle.js'))
    .pipe(rename('main.min.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(config.scripts.output));
})

gulp.task('build:html', function() {
	gulp.src(config.html.main)
	  .pipe(pug({
	  	pretty: true
    }))
    // .pipe(rename({
    //   extname: '.php'
    // }))
	  .pipe(gulp.dest(config.html.output))
});

gulp.task('build:images', function() {
	gulp.src(config.images.watch)
	  .pipe(imageop({
	  	optimizationLevel: 5,
	  	progressive: true,
	  	interlaced: true
	  }))
	  .pipe(gulp.dest(config.images.output))
});

gulp.task('watch', function() {
  // gulp.watch(config.images.watch, ['build:images'])
	gulp.watch(config.html.watch, ['build:html'])
	gulp.watch(config.styles.watch, ['build:css'])
	gulp.watch(config.scripts.watch, ['build:js'])
});

// gulp.task('build', ['build:html','build:css','build:js','build:images'])
gulp.task('build', ['build:html','build:css','build:js'])

gulp.task('default',['server','watch','build'])
