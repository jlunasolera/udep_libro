<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Udep</title>
</head>

<body style="margin:0;padding:0">
<table width="600" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
	<tr>
		<td style="padding:10px 20px;background-color:#063f7a">
			<img src="https://www.solera.pe/brand_udep.png" alt="logo">
		</td>
	</tr>
	<tr class="main">
		<td>
			<div class="message" style="padding:20px 100px 20px 20px;font-family:arial;color:#828282">
				<h4>Respuesta al reclamo Nº <span><?php echo @$code ?></span></h4>
      	<?php echo @$message; ?>
			</div>
		</td>
	</tr>
	<tr>
		<td class="footer" style="color:#828282;text-align:right">
			<div style="margin:0;padding:20px;list-style:none;font-size:9px;font-family:arial">
				<div>Universidad de Piura. Todos los derechos reservados.</div>
				<div>Piura: Av. Ramón Mugica 131, Urb. San Eduardo. T (073) 284500</div>
				<div>Lima: Calle Mártir José Olaya 162, Miraflores. T (01) 2139600</div>
				<div>Jr. Aldebarán N° 160, Santiago de Surco.</div>
			</div>
		</td>
	</tr>
</table>
</body>
</html>