window.jQuery = $ = require('jquery')

require('jquery-validation');
require('validate-method');
require('jlmodal');

$.fn.serializeObject = function() {
	var o = Object.create(null),
			elementMapper = function(element) {
				element.name = $.camelCase(element.name);
				return element;
			},
			appendToResult = function(i, element) {
				var node = o[element.name];

				if ('undefined' != typeof node && node !== null) {
					o[element.name] = node.push ? node.push(element.value) : [node, element.value];
				} else {
					o[element.name] = element.value;
				}
			};

	$.each($.map(this.serializeArray(), elementMapper), appendToResult);
	return o;
};

;(function ($) {

	$('input.form-control').on('click', function(e){
		// $(this).attr('aria-invalid','');
		// $(this).addClass('topAnimate');
		$(this).parent().addClass('topAnimate');
		// $(this).closest('input-group').addClass('topAnimate');
	})

	$("#address_station").on('change', function(){
		var thisopt = $(this).find("option:selected").attr('data-address');
		if(typeof thisopt === "undefined"){
			$("#address_station_text").text('');
		}else{
			$("#address_station_text").text(thisopt);
		}
	});

	$(".check-younger").on('change', function(){
		var younger = $('input[name="younger"]:checked', '#formRegister').val();

		if(younger == 1){
			$(".form-section-tutor").slideDown("fast");
			$("#typedoc_tutor").rules("add", "required");
			$("#numberdoc_tutor").rules("add", "required");
			$("#lastnamefa_tutor").rules("add", "required");
			$("#lastnamema_tutor").rules("add", "required");
			$("#name_tutor").rules("add", "required");
			$("#phone_tutor").rules("add", "required");
			$("#department_tutor").rules("add", "required");
			$("#province_tutor").rules("add", "required");
			$("#district_tutor").rules("add", "required");
			$("#address_tutor").rules("add", "required");
			$("#email_tutor").rules("add", "required");

			$("#typedoc_tutor").change(function(e){
				if($(this).val() == 1){
					$('#numberdoc_tutor').attr('maxlength',9);
					$('#lastnamema_tutor').rules('remove');
					$("#numberdoc_tutor").rules("remove", "maxlength minlength");
					$("#numberdoc_tutor").rules("add", {
						minlength:9,
						maxlength:9
					});
				}else{
					$('#numberdoc_tutor').attr('maxlength',8);
					$('#lastnamema_tutor').rules('add', {
						required: true,
					});
					$("#numberdoc_tutor").rules("remove", "maxlength minlength");
					$("#numberdoc_tutor").rules("add", {
						minlength:8,
						maxlength:8
					});
				}
			});

		} else {
			$(".form-section-tutor").slideUp("fast");
			$("#typedoc_tutor").rules("remove");
			$("#numberdoc_tutor").rules("remove");
			$("#lastnamefa_tutor").rules("remove");
			$("#lastnamema_tutor").rules("remove");
			$("#name_tutor").rules("remove");
			$("#phone_tutor").rules("remove");
			$("#department_tutor").rules("remove");
			$("#province_tutor").rules("remove");
			$("#district_tutor").rules("remove");
			$("#address_tutor").rules("remove");
			$("#email_tutor").rules("remove");
		}
	})

	// -------------------------------------------

	// $("#notify").on('change', function(){
	// 	var notify = $('input[name="notify"]:checked');

	// 	if(notify.length > 0){
	// 		$(".form-section-notify").slideUp("fast");
	// 		$("#address_notify").rules("remove");
	// 		$(this).val("1");
	// 	} else {
	// 		$(".form-section-notify").slideDown("fast");
	// 		$("#address_notify").rules("add", "required");
	// 		$(this).val("0");
	// 	}
	// })

	// -------------------------------------------

	$("#accordance").on('change', function(){
		var accordance = $('input[name="accordance"]:checked');

		if(accordance.length > 0){
			$(this).val("1");
		} else {
			$(this).val("0");
		}
	})

	$("#term").on('change', function(){
		var term = $('input[name="term"]:checked');

		if(term.length > 0){
			$('#submit').prop('disabled', false);
			$(this).val("1");
		} else {
			$('#submit').prop('disabled', true);
			$(this).val("0");
		}
	})

	$("#typedoc_child").change(function(e){
		if($(this).val() == 1){
			$('#numberdoc_child').attr('maxlength',9);
			$('#lastnamema_child').rules('remove');
			$("#numberdoc_child").rules("remove", "maxlength minlength");
			$("#numberdoc_child").rules("add", {
				minlength:9,
				maxlength:9
			});
		}else{
			$('#numberdoc_child').attr('maxlength',8);
			$('#lastnamema_child').rules('add', {
				required: true,
			});
			$("#numberdoc_child").rules("remove", "maxlength minlength");
			$("#numberdoc_child").rules("add", {
				minlength:8,
				maxlength:8
			});
		}
	});

	$('#department_child').change(function(){
		$.ajax({
			'url': site_url+'Home/listChildren',
			'type': 'POST',
			'data':{
				"id": $(this).val()
			},
			'success': function(data){
				$('#province_child').html('<option value></option>');
				for(i in data.ubigeo){
					provincia = data.ubigeo[i]
					$option = $('<option>');
					$option
						.attr('value',provincia.ubigeo_id)
						.text(provincia.name)
					;
					$('#province_child').append($option);
				}
			},
			'dataType':'json'
		});
	});

	$('#province_child').change(function(){
		$.ajax({
			'url': site_url+'Home/listChildren',
			'type': 'POST',
			'data':{
				"id": $(this).val()
			},
			'success': function(data){
				$('#district_child').html('<option value></option>');
				for(i in data.ubigeo){
					distrito = data.ubigeo[i]
					$option = $('<option>');
					$option
						.attr('value',distrito.ubigeo_id)
						.text(distrito.name)
					;
					$('#district_child').append($option);
				}
			},
			'dataType':'json'
		});
	});

	$('#department_tutor').change(function(){
		$.ajax({
			'url': site_url+'Home/listChildren',
			'type': 'POST',
			'data':{
				"id": $(this).val()
			},
			'success': function(data){
				$('#province_tutor').html('<option value></option>');
				for(i in data.ubigeo){
					provincia = data.ubigeo[i]
					$option = $('<option>');
					$option
						.attr('value',provincia.ubigeo_id)
						.text(provincia.name)
					;
					$('#province_tutor').append($option);
				}
			},
			'dataType':'json'
		});
	});

	$('#province_tutor').change(function(){
		$.ajax({
			'url': site_url+'Home/listChildren',
			'type': 'POST',
			'data':{
				"id": $(this).val()
			},
			'success': function(data){
				$('#district_tutor').html('<option value></option>');
				for(i in data.ubigeo){
					distrito = data.ubigeo[i]
					$option = $('<option>');
					$option
						.attr('value',distrito.ubigeo_id)
						.text(distrito.name)
					;
					$('#district_tutor').append($option);
				}
			},
			'dataType':'json'
		});
	});

	$('#submit').on('click', function(e){
		var response = grecaptcha.getResponse();
		$(this).prop('disabled', true);

		if(response == ""){
			$(this).prop('disabled', false);
			$('.recaptcha-error').show();
    } else {
			$('#formRegister').submit();
    }
	});

	// -------------------------------------------

	// $("#notify").on('change', function(){
	// 	var notify = $('input[name="notify"]:checked');

	// 	if(notify.length > 0){
	// 		$(".form-section-notify").slideUp("fast");
	// 		$("#address_notify").rules("remove");
	// 		$(this).val("1");
	// 	} else {
	// 		$(".form-section-notify").slideDown("fast");
	// 		$("#address_notify").rules("add", "required");
	// 		$(this).val("0");
	// 	}
	// })

	$('#typedoc_child').on('change', function(e){
		console.log($(this).find('option:selected').val());
	})

	// -------------------------------------------

	$('#formRegister').validate({
		rules: {
			typedoc_child: {
				required: true
			},
			numberdoc_child: {
				required: true,
				number: true,
				minlength:8,
				maxlength:9
			},
			lastnamefa_child: {
				required: true
			},
			lastnamema_child: {
				required: true
			},
			name_child: {
				required: true
			},
			phone_child: {
				required: true,
				number: true,
				minlength:6,
				maxlength:9
			},
			department_child: {
				required: true
			},
			province_child: {
				required: true
			},
			district_child: {
				required: true
			},
			address_child: {
				required: true
			},
			email_child: {
				required: true,
				email: true
			},
			younger: {
				required: true
			},
			numberdoc_tutor: {
				number: true,
				minlength:8,
				maxlength:9
			},
			phone_tutor: {
				number: true,
				minlength:6,
				maxlength:9
			},
			email_tutor: {
				email: true
			},
			typeservice: {
				required: true
			},
			claim_amount: {
				required: true
			},
			address_station: {
				required: true
			},
			typeservice_description: {
				required: true
			},
			complain: {
				required: true
			},
			complain_detail: {
				required: true
			},
			complain_order: {
				required: true
			},
			term: {
				required: true
			},
			accordance: {
				required: true
			},
			notify: {
				required: true
			}
		},
		errorPlacement: function(error,element) {
			$("#submit").prop('disabled', false);
			return true;
		},
		highlight: function(element, errorClass){
			$(element).closest('.print-err').addClass('has-error');
		},
		unhighlight: function(element, errorClass){
			$(element).closest('.print-err').removeClass('has-error');
		},
		submitHandler: function(form){
			// console.log("enviado")
			if(grecaptcha.getResponse() != ''){
				$('.recaptcha-error').hide();
				$('#submit').off('click');
				sendData(form);
			}else{
				$('.recaptcha-error').show();
			}
		}
	});

})(jQuery);

// $('#btnRegister').click();

function sendData(form){
	var dataObj = JSON.stringify( $(form).serializeObject() );

	$.ajax({
		type: "POST",
		url: "http://servicelibro.udep.edu.pe/v1/register",
		processData: false,
		dataType: 'json',
		data: dataObj,
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		success: function (data) {
			// console.log(data);
			if (data.success) {
				console.log("envio");
				var code_orden = data.body.data.code;
				window.location.href = site_url+"gracias/"+code_orden;
			} else {
				console.log("no envio")
				$(this).prop('disabled', false);
			}
		}
	});
}

// function createRecaptcha() {
// 	grecaptcha.render("recaptcha", {sitekey: "6LeMOy4UAAAAAEvfDM80vM1CPaN6R83KQPOUaOEr", theme: "light"});
// }
// createRecaptcha();