;(function ($) {
	// clic en la vista reporte
	$('.complaint-view').on('click', function(e){
		$(this).prop('disabled', true);
		var $id = $(this).parent().parent().find('input').val();
		// viewComplaint($id);
		$('#myModal').modal('show')
	})

	// clic en la vista usuario
	$('.userDelete').on('click', function(e){
		$(this).prop('disabled', true);
		if(confirm("Esta seguro que deseas eliminar el usuario")){
			var $id = $(this).parent().parent().find('input').val();
			deleteUser($id);
		}else{
			$(this).prop('disabled', false);
		}
	})

	// clic en envio de corr

	$('.userResetPass').on('click', function(e){
		$(this).prop('disabled', true);
		if(confirm("Esta seguro que deseas eliminar el usuario")){
			var $id = $(this).parent().parent().find('input').val();
			changePassword($id);
		}else{
			$(this).prop('disabled', false);
		}
	})

	$('.userStatus').on('click', function(e){
		$(this).prop('disabled', true);
		var $status = $(this).attr('data-status');
		if(confirm("Esta seguro que deseas cambiar el estado")){
			var $id = $(this).parent().parent().find('input').val();
			userStatus($id,$status);
		}else{
			$(this).prop('disabled', false);
		}
	})

	$('#btnNewRegister').on('click', function(e){
    $('#newRegister').submit();
    $("#btnNewRegister").prop('disabled', true);
  })
  
  $('#newRegister').validate({
		rules: {
			newuser_name: {
				required: true
			},
			newuser_lastname: {
				required: true
			},
			newuser_email: {
				required: true,
				email: true
			},
			newuser_typerol: {
        required: true
			}
		},
		errorPlacement: function(error,element) {
			$("#btnNewRegister").prop('disabled', false);
			return true;
		},
		highlight: function(element, errorClass){
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass){
			$(element).closest('.form-group').removeClass('has-error');
		},
		submitHandler: function(form){
			sendRegisterUser(form);
		}
	});

})(jQuery);

function sendRegisterUser(form){
	$.ajax({
		url: site + 'dashboard/visitors/newUserValidate',
		type:'post',
		data: $(form).serialize(),
		success: function(data){
			data = JSON.parse(data);
			if (data.print){
        console.log("se guardo");
        $(form)[0].reset();
        $("#btnNewRegister").prop('disabled', false);
				location.reload();
			} else {
        console.log("no se guardo");
        $("#btnNewRegister").prop('disabled', false);
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}


function userStatus(id,status){
	PNotify.prototype.options.styling = "bootstrap3";

	$.ajax({
		url: site + 'dashboard/visitors/userStatus',
		type:'post',
		dataType: "json",
		data: {
			"id": id,
			"status": status
		},
		success: function(data){
			if (data.status){
				$('#tr'+id).find('.statusValue').text(data.value);
				$('#tr'+id).find('.userStatus').attr('data-status', data.value_num);
				$('#tr'+id).find('.userStatus').prop('disabled', false);
				new PNotify({
					title: 'Correcto',
					text: data.message,
					type: 'success'
				});
			} else {
				new PNotify({
					title: 'Error',
					text: data.message,
					type: 'error'
				});
			}
		},
		error: function(jqhxr, textStatus, error){
			new PNotify({
				title: 'Error',
				text: data.message,
				type: 'error'
			});
		}
	});
}

function changePassword(id){
	PNotify.prototype.options.styling = "bootstrap3";
	$.ajax({
		url: site + 'dashboard/visitors/changePassword',
		type:'post',
		dataType: "json",
		data: {
		  "id": id
		},
		success: function(data){
			if (data.status){
				$('.userResetPass').prop('disabled', false);
				new PNotify({
					title: 'Correcto',
					text: data.message,
					type: 'success'
				});
			} else {
				$('.userResetPass').prop('disabled', false);
        new PNotify({
					title: 'Error',
					text: data.message,
					type: 'error'
				});
			}
		},
		error: function(jqhxr, textStatus, error){
			new PNotify({
				title: 'Error',
				text: data.message,
				type: 'error'
			});
		}
	})
}

function deleteUser(id){
	PNotify.prototype.options.styling = "bootstrap3";
	$.ajax({
		url: site + 'dashboard/visitors/delete_user',
		type:'post',
		dataType: "json",
		data: {
		  "id": id
		},
		success: function(data){
			if (data.status){
				$('#tr'+id).html('');
				new PNotify({
					title: 'Correcto',
					text: data.message,
					type: 'success'
				});
			} else {
        new PNotify({
					title: 'Error',
					text: data.message,
					type: 'error'
				});
			}
		},
		error: function(jqhxr, textStatus, error){
			new PNotify({
				title: 'Error',
				text: data.message,
				type: 'error'
			});
		}
	});
}

function viewComplaint(id){
	$.ajax({
		url: site + 'dashboard/visitors/delete_user',
		type:'post',
		dataType: "json",
		data: {
		  "id": id
		},
		success: function(data){
			if (data.status){
				$('#tr'+id).html('');
			} else {
        console.log("no se elimino");
			}
		},
		error: function(jqhxr, textStatus, error){
			console.log('error3: '+ data.message);
			console.log('error en el envio');
		}
	});
}