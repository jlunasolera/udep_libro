<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

$route['pad'] = 'home/pad';
$route['udep'] = 'home';

$route['dashboard'] = "dashboard/login";

$route['get_comments/(:num)/(:num)'] = "home/get_comments/$1/$2";
//Que Quieres Ser
$route['dashboard/index'] = "dashboard/visitors/index";
$route['dashboard/index/(.+)'] = "dashboard/visitors/index/$1";
$route['dashboard/reportes'] = "dashboard/reports/index";
$route['dashboard/usuarios'] = "dashboard/visitors/users";
$route['dashboard/usuarios/(.+)'] = "dashboard/visitors/users/$1";
$route['dashboard/respuesta_reclamo'] = "dashboard/visitors/complaint_form";
$route['dashboard/remover_archivos'] = "dashboard/visitors/removeFilesSave";
$route['dashboard/graficos'] = "dashboard/visitors/graphic";
$route['dashboard/index/export'] = "dashboard/visitors/export";

// $route['mailresponse'] = "dashboard/visitors/mailResponse";

// dropzone
$route['complaint_add_files/post']['post'] = 'dashboard/visitors/complaintAddFiles';
$route['complaint_add_files_orden/(.+)'] = 'dashboard/visitors/complaintAddFilesOrden/$1';
//Proponiendo un Cambio
/*
$route['dashboard/proponiendo-un-cambio'] = "dashboard/visitors/index";
$route['dashboard/proponiendo-un-cambio/index/(.+)'] = "dashboard/visitors/index/$1";
$route['dashboard/proponiendo-un-cambio/export'] = "dashboard/visitors/export";*/

// Difunde Form

$route['gracias/(:num)'] = 'home/gracias/$1';

// $route['mail'] = 'home/mail_view';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
