<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>UDEP - UNIVERSIDAD DE PIURA - HOJA DE RECLAMACIÓN</title>
    <?php /*<meta property="og:url"           content="http://www.movimientoprimor.pe/" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="En Movimiento con Primor 4K" />
    <meta property="og:description"   content="Te esperamos en el Primer MOVIMIENTO PRIMOR 4K. Con cada km recorrido, ayudaremos a las familias que más lo necesitan" />
    <meta property="og:image"         content="<?php echo site_url('static/web/dist'); ?>/images/share.jpg" /> */ ?>
    <script>
      var createRecaptcha = function () {
        grecaptcha.render("recaptcha", {sitekey: "6LeMOy4UAAAAAEvfDM80vM1CPaN6R83KQPOUaOEr", theme: "light"});
      }
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=createRecaptcha&render=explicit" async defer></script>
    <link type="text/css" rel="stylesheet" href="<?php echo site_url('static/web/dist'); ?>/css/main.min.css">
  </head>
  <body>
    <?php echo $body; ?>
    <script>
      var site_url = "<?php echo site_url(); ?>";
    </script>
    <script src="<?php echo site_url('static/web/dist'); ?>/js/main.min.js"></script>
  </body>
</html>