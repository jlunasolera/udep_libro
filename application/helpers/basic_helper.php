<?php

function get_semilla(){
	$semilla='40480800';
	return $semilla;
}

function generar_codigo($pk_usuario){
	if ($pk_usuario < 20){
		$resto = 12345678;
	}else if ($pk_usuario <= 217){
		$resto =1234567;
	}else if ($pk_usuario <= 2176){
		$resto =123456;
	}else if ($pk_usuario <= 21767){
		$resto =12345;
	}else if ($pk_usuario <= 217678){
		$resto =1234;
	}else{
		$resto =123;
	}
	$codigo = $pk_usuario.$resto;
	return $codigo;
}

function enviarMail($from,$to,$bcc='',$asunto,$mensaje,$nombre='',$attachments=FALSE){

	$CI =& get_instance();
	$CI->load->library('email');
	$config['charset'] = 'utf-8';
	$config['wordwrap'] = TRUE;
	$config['mailtype'] = "html";
	$CI->email->initialize($config);

	$CI->email->clear();

	$CI->email->to($to);
	if($bcc != "")
		$CI->email->bcc($bcc);
	$CI->email->from($from, $nombre);
	$CI->email->subject($asunto);
	$CI->email->message($mensaje);

	if ($attachments) {
		foreach ($attachments as $attach) {
			$CI->email->attach($attach);
		}
	}

	if( $CI->email->send())
		return true;

	else
		return false;
	//echo $CI->email->print_debugger();
}

function date_to_string(){
	date_default_timezone_set("America/Lima");
	$date = date('d/m/Y');
	$fecha_separada=explode("/", $date);

	$week_day = weekday(date("N"));
	//$dia= strtolower(numtoletras($fecha_separada[0]));
	$dia = $fecha_separada[0];
	switch ($fecha_separada[1]) {

		case "01":
			$mes="Enero";
			break;
		case "02":
			$mes="Febrero";
			break;
		case "03":
			$mes="Marzo";
			break;
		case "04":
			$mes="Abril";
			break;
		case "05":
			$mes="Mayo";
			break;
		case "06":
			$mes="Junio";
			break;
		case "07":
			$mes="Julio";
			break;
		case "08":
			$mes="Agosto";
			break;
		case "09":
			$mes="Septiembre";
			break;
		case "10":
			$mes="Octubre";
			break;
		case "11":
			$mes="Noviembre";
			break;
		case "12":
			$mes="Diciembre";
			break;

		default:
			break;
	}

	//$anio= strtolower(numtoletras($fecha_separada[2]));
	$anio= $fecha_separada[2];
	return "$week_day $dia de $mes de $anio";
}

function weekday($dia){
	switch ($dia) {
		case 7:
			$dia_semana = "Domingo";
			break;
		case 1:
			$dia_semana = "Lunes";
			break;
		case 2:
			$dia_semana = "Martes";
			break;
		case 3:
			$dia_semana = "Miercoles";
			break;
		case 4:
			$dia_semana = "Jueves";
			break;
		case 5:
			$dia_semana = "Viernes";
			break;
		case 6:
			$dia_semana = "Sabado";
			break;
	}
	return $dia_semana;
}

function slug($string){
	$characters = array(
		"Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u", "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u", "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
	);
	
	$string = strtr($string, $characters);
	$string = strtolower(trim($string));
	$string = preg_replace('/[^a-z0-9-]/', "-", $string);
	$string = preg_replace("/-+/", "-", $string);

	if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
		$string = substr($string, 0, strlen($string) - 1);
	}
	
	return $string;
}