<section class="content-header">
	<h1>Dashboard</h1>
  <div class="breadcrumb">
  	<a style="width: 100%;" class="tipo btn btn-primary" href="<?php echo site_url().'dashboard/'.$this->uri->segment(2).'/export' ?>"  title="Exportar Excel" >
				<span class="btn-label icon fa fa-plus" ></span>&nbsp;&nbsp;Exportar</a>
  </div>
</section>

<section class="content">
	<div class="row">

		<div class="col-md-2 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red bg-pad"><img src="<?php echo site_url(); ?>static/cms/images/logo_pad_blanco.png" alt="PAD"></span>
				<div class="info-box-content">
					<span class="info-box-text">Surco</span>
					<span class="info-box-number"><?php echo isset($obj_querysurco)?$obj_querysurco:0 ?></span>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-sm-6 col-xs-12">
			<div class="info-box">
				<!-- <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span> -->
				<span class="info-box-icon bg-red bg-udep"><img src="<?php echo site_url(); ?>static/cms/images/logo_udep_blanco.png" alt="UDEP"></span>

				<div class="info-box-content">
					<span class="info-box-text">Miraflores</span>
					<span class="info-box-number"><?php echo isset($obj_querymiraflores)?$obj_querymiraflores:0 ?></span>
				</div>
			</div>
		</div>

		<div class="clearfix visible-sm-block"></div>

		<div class="col-md-2 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red bg-udep"><img src="<?php echo site_url(); ?>static/cms/images/logo_udep_blanco.png" alt="UDEP"></span>

				<div class="info-box-content">
					<span class="info-box-text">Piura</span>
					<span class="info-box-number"><?php echo isset($obj_querypiura)?$obj_querypiura:0 ?></span>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red bg-udep"><img src="<?php echo site_url(); ?>static/cms/images/logo_udep_blanco.png" alt="UDEP"></span>

				<div class="info-box-content">
					<span class="info-box-text">Chiclayo</span>
					<span class="info-box-number"><?php echo isset($obj_querychiclayo)?$obj_querychiclayo:0 ?></span>
				</div>
			</div>
		</div>

		<div class="clearfix visible-sm-block"></div>

		<div class="col-md-2 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red bg-udep"><img src="<?php echo site_url(); ?>static/cms/images/logo_udep_blanco.png" alt="UDEP"></span>

				<div class="info-box-content">
					<span class="info-box-text">Trujillo</span>
					<span class="info-box-number"><?php echo isset($obj_querytrujillo)?$obj_querytrujillo:0 ?></span>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Listado de reclamos</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Fecha Recepción</th>
							<th>Fecha Resolución</th>
							<th>Nª de Reclamo</th>
							<th>Sede</th>
							<th>Estado</th>
							<th>Usuario de atención</th>
							<th>Tipo de servicio</th>
							<th>Respuesta</th>
						</tr>
						</thead>
						<tbody>
						<?php if(isset($obj_visitors) && count($obj_visitors) > 0){
							foreach($obj_visitors as $obj_visitor){?>
								<tr class="gradeA odd">
									<td><?php echo date("d/m/Y", strtotime($obj_visitor->date_creation)) ?></td>
									<td><?php 
										if($obj_visitor->date_resolution != '-'){
											echo date("d/m/Y", strtotime($obj_visitor->date_resolution));
										} else {
											echo '-';
										} ?>
									</td>
									<td><?php echo $obj_visitor->code ?></td>
									<td><?php echo $obj_visitor->address_station ?></td>
									<td>
										
										<?php switch($obj_visitor->status) {
											case 0:
												echo '<span class="label label-info">Pendiente</span>';
												break;
											case 1:
												echo '<span class="label label-primary">Finalizado</span>';
												break;
										} ?>
									</td>
									<td><?php echo $obj_visitor->user_name ?></td>
									<td><?php echo $obj_visitor->typeservice == 0 ? 'Producto':'Servicio' ?></td>
									<td>
										<a class="btn btn-sm btn-danger complaint-view complaint-response" data-id="<?php echo $obj_visitor->id ?>">
											<span class="btn-label icon fa fa-file-text" aria-hidden="true"></span>
										</a>
									</td>
								</tr>
							<?php }} ?>
						</tbody>
					</table>
					<?php if(isset($obj_paginacion) && empty($obj_paginacion) != 1) {?>
						<div class="box-footer clearfix">
							<ul class="pagination pagination-sm no-margin pull-right"><?php echo $obj_paginacion ?></ul>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- modal vista -->
<div id="modalComplaintResponse" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Reclamo nº <span id="complaintResponse_code"></span></h4>
			</div>
			<div class="modal-body">
				<div class="modal-body-box">
					<table class="report-table">
						<tbody>
							<tr>
								<td>
									<table class="table">
										<tr id="complaintResponse_name">
											<td class="report-table-label"><h4>Nombre completo:</h4></td>
											<td><p class="responseComplaintId"></p></td>
										</tr>
										<tr id="complaintResponse_email">
											<td class="report-table-label"><h4>E-mail:</h4></td>
											<td><p class="responseComplaintId"></p></td>
										</tr>
										<tr id="complaintResponse_dateregister">
											<td class="report-table-label"><h4>Fecha de registro:</h4></td>
											<td><p class="responseComplaintId"></p></td>
										</tr>
										<tr id="complaintResponse_typesend">
											<td class="report-table-label"><h4>Tipo de envío:</h4></td>
											<td><p class="responseComplaintId"></p></td>
										</tr>
										<tr id="complaintResponse_filepdf">
											<td class="report-table-label"><h4>Pdf de reclamo:</h4></td>
											<td><p class="responseComplaintId"><span class="label label-primary bg-teal"><a href="" target="_blank"></a></span></p></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="complaintModal-title">
								<td colspan="2">
									<h3>Respuesta al reclamo</h3>
								</td>
							</tr>
							<tr>
								<td>
									<table class="table" id="tableResponseStatus">
										<tr id="complaintResponse_cco">
											<td class="report-table-label"><h4>CCO:</h4></td>
											<td>
												<input type="hidden" name="complaint_id" value>
												<input type="hidden" name="complaint_code" value>
												<input type="hidden" name="complaint_userid" value="<?php echo $_SESSION['usercms']['user_id']; ?>">
												<input type="hidden" name="complaint_emailchild" value>
												<input type="hidden" name="complaint_typesend" value>
												<div>
													<textarea name="copyhidden" id="copyhidden" cols="30" rows="2" placeholder="example@mail.com, example2@mail.com, ..." class="form-control"></textarea>
												</div>
											</td>
										</tr>
										<tr id="complaintAddFiles" class="filesDropzoneDesign" data-id="">
											<td class="report-table-label" id="actions" colspan="2">
												<h4 class="titleSectionFull">Archivos adjuntos en respuesta:</h4>
												<div id="btnFilesAdd">
													<span class="btn btn-info fileinput-button">
														<i class="glyphicon glyphicon-plus"></i>
														<span>Agregar archivos...</span>
													</span>
												</div>

												<div>
													<!-- The global file processing state -->
													<span class="fileupload-process" style="display:none">
														<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
															<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
														</div>
													</span>
												</div>
												<div class="table files table-files" id="previews">
													<div id="template" class="file-row">
														<table class="table table-dropzone">
															<tr>
																<td>
																	<p class="size" data-dz-size></p>
																	<p class="name" data-dz-name></p>
																	<strong class="error text-danger" data-dz-errormessage></strong>
																</td>
																<td>
																	<button class="btn btn-primary start" style="display:none">
																		<i class="glyphicon glyphicon-upload"></i>
																	</button>
																	<button data-dz-remove class="btn btn-warning cancel" style="display:none">
																		<i class="glyphicon glyphicon-ban-circle"></i>
																	</button>
																	<button data-dz-remove class="btn btn-danger delete">
																		<i class="glyphicon glyphicon-trash"></i>
																	</button>
																</td>
															</tr>
														</table>
													</div>
												</div>
												<div class="table files table-files" id="previews-save">
												</div>
											</td>
										</tr>
										<tr id="report-service">
											<td class="report-table-label" colspan="2">
												<h4 class="titleSectionFull">Mensaje de respuesta:</h4>
												<div id="complaintResponseMailBox">
													<textarea id="complaintResponseMail" name="complaint_responsemail" rows="3" ></textarea>
												</div>
												<div id="complaintResponseMailHtml" style="display:none"></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="complaintAction btn btn-warning btn-rejected btn-action-complaint" data-action="0"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
				<button type="button" class="complaintAction btn btn-danger btn-rejected btn-action-complaint" data-action="2"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Rechazado</button>
				<button type="button" class="complaintAction btn btn-success btn-proceeds btn-action-complaint" data-action="1"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Procede</button>
			</div>
			<!-- ini input hidden -->
			<!-- <input type="hidden" id="report-id" value="">
			<input type="hidden" id="login-id" value="<?php echo $_SESSION['usercms']['user_id']; ?>">
			<input type="hidden" id="login-name" value="<?php echo $_SESSION['usercms']['name']; ?>"> -->
			<!-- end input hidden -->

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<?php echo site_url() ?>static/cms/js/jquery.validate.min.js"></script>
<!-- CK Editor -->
<script src="<?php echo site_url()?>static/cms/lib/ckeditor/ckeditor.js"></script>
<script src="<?php echo site_url()?>static/cms/lib/dropzone/dropzone.js"></script>
<script src="<?php echo site_url() ?>static/cms/js/visitors.js"></script>
