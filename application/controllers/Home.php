<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// Set local time zone
		date_default_timezone_set("America/Lima");
		$this->load->model('visitor_model', 'obj_visitor');
		$this->load->model('libro_model', 'obj_libro');
		$this->load->model('ubigeo_model', 'objUbigeo');
	}

	public function index(){
		$objUbigeo = $this->objUbigeo->listParent();
		$objSedes = $this->obj_libro->list_sedes();

		$this->tmp_master->set('objSedes', $objSedes);
		$this->tmp_master->set('objUbigeo', $objUbigeo);
		$this->tmp_master->render('index');
	}

	public function pad(){
		$objUbigeo = $this->objUbigeo->listParent();

		$this->tmp_master->set('objUbigeo', $objUbigeo);
		$this->tmp_master->render('pad');
	}

	public function listChildren() {
		$id = $this->input->post('id');
		$data['ubigeo'] = $this->objUbigeo->listUbigeot($id);
		echo json_encode($data);
	}

	public function gracias($order){
		$data["num_order"] = $order;
		
		$obj_responses = $this->obj_libro->select_reclamo($order);

		if ( count($obj_responses) > 0 ) {
			// for ($i=0; $i < count($rs); $i++) {
				
			// 	$data["institution"] = $rs[$i]->institution == 0 ? 'udep' : 'pad';
			// }
			foreach($obj_responses as $obj_response) {
				$data["url_pdf"] = $obj_response->url_pdf;
				$data["institution"] = $obj_response->institution == 0 ? 'udep' : 'pad';
				// $typeserv = $obj_registro->typeservice == 0 ? "Producto":"Servicio";
			}
		}

		$this->tmp_master->set('data', $data);
		$this->tmp_master->render('gracias');
	}

}