<?php
class Datatables_report_model extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  public function allposts_count(){
    $query = $this
              ->db
              ->get('reclamo');
    return $query->num_rows();
  }
    
  public function allposts($limit,$start,$col,$dir){
    $query = $this
              ->db
              ->select("id, code, CONCAT(name_child,' ',lastnamefa_child,' ',lastnamema_child) AS name_complete, email_child,complain, sedes.name AS address_station, typeservice, CASE WHEN CONCAT(user.name,' ',user.lastname) IS NULL THEN '-' ELSE CONCAT(user.name,' ',user.lastname) END AS user_id, sub_status, reclamo.status",FALSE)
              ->limit($limit,$start)
              ->order_by($col,$dir)
              ->join('sedes', 'sedes.idSedes = reclamo.address_station')
              ->join('user', 'user.user_id = reclamo.user_id', 'left')
              ->get('reclamo');

    // $sql='SELECT id, name_child, lastnamefa_child, lastnamema_child, email_child, date_creation, date_resolution, code, sedes.name AS address_station, reclamo.status, (select CONCAT(user.name, " ", user.lastname) FROM `user` WHERE `user`.`user_id` = `reclamo`.`user_id`) AS user_name, typeservice FROM (`reclamo`) JOIN `sedes` ON `sedes`.`idSedes` = `reclamo`.`address_station` ORDER BY `date_creation` DESC LIMIT 10';
    
    // $query = $this->db->query($sql);

    if($query->num_rows()>0){
      return $query->result();
    } else {
      return null;
    }
  }

  public function posts_search($limit,$start,$search,$col,$dir){
    $query = $this
              ->db
              ->select("*")
              ->like('correlativo',$search)
              ->or_like('destinatario',$search)
              ->or_like('identificador',$search)
              ->limit($limit,$start)
              ->order_by($col,$dir)
              ->get('reclamo');
    
    if($query->num_rows()>0){
      return $query->result();
    } else {
      return null;
    }
  }

  public function posts_search_count($search) {
    $query = $this
              ->db
              ->select("*")
              ->like('correlativo',$search)
              ->or_like('destinatario',$search)
              ->or_like('identificador',$search)
              ->get('reclamo');
    
    return $query->num_rows();
  }
 
}